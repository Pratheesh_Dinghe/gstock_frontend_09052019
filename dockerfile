# ### STAGE 1: Build ###

# # We label our stage as 'builder'
# FROM node:8-alpine as builder

# COPY package.json package-lock.json ./

# RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

# ## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
# RUN npm i && mkdir /ng-app && cp -R ./node_modules ./ng-app

# WORKDIR /ng-app
# COPY ./skycons ./node_modules/skycons
# COPY ./pace ./node_modules/pace
# COPY . .
# RUN $(npm bin)/ng build
# ### STAGE 2: Setup ###

FROM nginx:alpine

## Copy our default nginx config
COPY reverseproxy/nginx.conf /etc/nginx

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From 'builder' stage copy over the artifacts in dist folder to default nginx public folder
COPY /dist /usr/share/nginx
VOLUME [ "/server/dist/uploads" ]
CMD ["nginx", "-g", "daemon off;"]