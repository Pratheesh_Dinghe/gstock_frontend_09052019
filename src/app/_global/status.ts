export const DELIVERY_STATUS = {
    'GR': 'Good received',
    'AD': 'Awaiting delivery',
    'Paid': 'Payment made',
    'Pending': 'Pending payment',
    'DD': 'Delivering',
};
