
export function addTimestamp(url: string) {
    const src = url.lastIndexOf('?ts=') > 0 ? `${url.substr(0, url.lastIndexOf('?ts='))}` : url;
    return `${src}?ts=${Date.now()}`;
}
