import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '../app.state';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    constructor(private injector: Injector) { }
    handleError(error) {
        console.log('GlobalErrorHandler', error);
        if (error instanceof HttpErrorResponse) {
            const appState = this.injector.get(AppState);
            appState.notifyDataChanged('http_error', error);
        }

    }

}
