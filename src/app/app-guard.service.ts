import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { AuthService } from './shared/auth.service';

import { Injectable, } from '@angular/core';
import {
    CanLoad,
    CanActivate, Router, Route,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';

@Injectable()
export class AppGuard implements CanLoad {
    constructor(
        public _authService: AuthService,
        public _router: Router) {

    }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        // Store the attempted URL for redirecting
        this._authService.redirectUrl = `/${route.path}`;
        return this._authService.authState().map(
            (auth) => {
                console.log('Access ok. Navigating to', this._authService.redirectUrl);
                // this._router.navigateByUrl(this._authService.redirectUrl);
                return true;
            }).catch(
                err => {
                    console.log('Access not ok', err);
                    this._router.navigate(['/login']);
                    return Observable.of(false);
                });
    }
    // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|boolean {
}
