import { Component, ViewEncapsulation } from '@angular/core';
import { AppState } from 'app/app.state';
import { Router } from '@angular/router';

@Component({
  selector: 'az-root',
  encapsulation: ViewEncapsulation.None,
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor(
    private _appState: AppState,
    private _router: Router
  ) {
   _appState.subscribe('login', (success) => {
      if (success) {
        console.log('Once');
        this._router.navigate(['/pages']);
      } else {
        this._router.navigate(['/login']);
      }
    });
    _appState.subscribe('http_error', (err) =>{
        this._router.navigate(['/error']);
    })
  }

}
