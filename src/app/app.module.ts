import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './shared/auth.service';
import 'pace';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AgmCoreModule } from '@agm/core';

import { routing } from './app.routing';
import { AppConfig } from './app.config';

import { AppComponent } from './app.component';
import { ErrorComponent } from './pages/error/error.component';
import { HttpModule, JsonpModule } from '@angular/http';
import { AppGuard } from './app-guard.service';
import * as $ from 'jquery';
import { AppState } from 'app/app.state';
import { NgDatepickerModule } from 'ng2-datepicker';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent

  ],
  imports: [
    HttpClientModule,
    HttpModule,
    JsonpModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgDatepickerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDe_oVpi9eRSN99G4o6TwVjJbFBNr58NxE'
    }),
    routing
  ],
  providers: [
      AppConfig,
      AuthService,
      AppGuard,
      AppState,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
