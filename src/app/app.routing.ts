import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { ErrorComponent } from './pages/error/error.component';
import { AppGuard } from './app-guard.service';
export const routes: Routes = [



  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  {
    path: 'pages', loadChildren: 'app/pages/pages.module#PagesModule',
    canLoad: [AppGuard],
  },
  { path: 'login', loadChildren: 'app/pages/login/login.module#LoginModule' },
  { path: 'register', loadChildren: 'app/pages/register/register.module#RegisterModule' },
  {path: 'error', component: ErrorComponent},
  {path: '**', redirectTo: 'error'}
];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
  preloadingStrategy: PreloadAllModules,
  // useHash: true
});
