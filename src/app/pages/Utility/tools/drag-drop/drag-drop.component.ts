import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DragulaService } from 'ng2-dragula/components/dragula.provider';

@Component({
  selector: 'az-drag-drop',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.scss']
})
export class DragDropComponent implements OnInit {
  private items = [1, 2, 3, 4, 5, 6]
  constructor(private _dragulaService: DragulaService) { }

  ngOnInit() {
    this._dragulaService.dropModel.subscribe((bagName, el, target, source) => {
      console.log(bagName, el, target, source, this.items)
    })
  }

}
