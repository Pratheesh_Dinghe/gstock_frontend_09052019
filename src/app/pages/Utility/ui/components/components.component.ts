import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'az-components',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './components.component.html'
})
export class ComponentsComponent {
    ngOnInit(): void {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    }
}
