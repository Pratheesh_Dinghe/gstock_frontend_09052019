import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user/user.service';
import { DatePipe } from '@angular/common';
import { LocalDataSource } from 'ng2-smart-table';
import { errorHandler } from '../../_helper/errorHandler';
import * as flatten from 'flatten-obj';
import { Router } from '@angular/router';
@Component({
    selector: 'gs-credit-management',
    templateUrl: 'credit-management.component.html'
})

export class CreditManagementComponent implements OnInit {
    private source = new LocalDataSource();
    private settings = {
        actions: {
            delete: false,
            edit: false,
            add: false
        },
        columns: {
            'buyer_id.credential.email': {
                title: 'Email'
            },
            'buyer_id.profile.first_name': {
                title: 'Buyer'
            },
            'updatedAt': {
                title: 'Last Updated At',
                valuePrepareFunction: (date) => {
                    const raw = new Date(date);
                    const formatted = new DatePipe('en-EN').transform(raw, 'dd-MM-yyyy HH:mm');
                    return formatted;
                }
            },
            'total': {
                title: 'Credit Amount',
            }
        }
    };
    constructor(
        private _userService: UserService,
        private _router: Router
    ) {
        console.log('callll');
        _userService.getCreditWalletList().subscribe(
            (data: any) => {
                const flattened = data.map(d => flatten()(d));
                console.log(flattened);
                this.source.load(flattened);
            },
            err => errorHandler(err)
        );
    }

    ngOnInit() {

    }

    toBuyer(row) {
        this._router.navigate(['/pages/usermanagement/buyer/detail', row.data['buyer_id._id']]);
    }
}
