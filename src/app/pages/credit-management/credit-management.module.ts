import { NgModule } from '@angular/core';

export const routes = [
    { path: '', component: CreditManagementComponent, pathMatch: 'full' }
];
import { CreditManagementComponent } from './credit-management.component';
import { RouterModule } from '@angular/router';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UserService } from '../user/user/user.service';
@NgModule({
    imports: [
        RouterModule.forChild(routes),
        Ng2SmartTableModule,
        ReactiveFormsModule,
        FormsModule
    ],
    exports: [],
    declarations: [CreditManagementComponent],
    providers: [UserService],
})

export class CreditManagementModule { }
