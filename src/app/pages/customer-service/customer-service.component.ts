import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user/user.service';
import { DatePipe } from '@angular/common';
import { LocalDataSource } from 'ng2-smart-table';
import { errorHandler } from '../../_helper/errorHandler';
import * as flatten from 'flatten-obj';
import { Router } from '@angular/router';
import { OrderService } from '../order/order.service';

@Component({
    selector: 'gs-customer-service',
    templateUrl: 'customer-service.component.html'
})

export class CustomerServiceComponent implements OnInit {
    private source = new LocalDataSource();
    private settings = {
        actions: {
            delete: false,
            edit: false,
            add: false
        },
        columns: {
            'buyer_id': {
                title: 'Buyer'
            },
            'order_id': {
                title: 'Order'
            },
            'comment': {
                title: 'Comment'
            },
            'createdAt': {
                title: 'Issued At',
                valuePrepareFunction: (date) => {
                    const raw = new Date(date);
                    const formatted = new DatePipe('en-EN').transform(raw, 'dd-MM-yyyy HH:mm');
                    return formatted;
                }
            }
        }
    };
    constructor(
        private _orderService: OrderService
    ) {
        _orderService.getComplaint().subscribe(
            (res: any) => {
                const flattened = res.map(d => flatten()(d));
                this.source.load(flattened);
            });
    }

    ngOnInit() { }

    toBuyer(e){

    }
}
