import { NgModule } from '@angular/core';

import { CustomerServiceComponent } from './customer-service.component';
import { RouterModule } from '@angular/router';
import { OrderService } from '../order/order.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
export const routes = [
    { path: '', component: CustomerServiceComponent, pathMatch: 'full' }
];
@NgModule({
    imports: [
        RouterModule.forChild(routes),
        Ng2SmartTableModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [],
    declarations: [
        CustomerServiceComponent
    ],
    providers: [OrderService],
})
export class CustomerServiceModule { }
