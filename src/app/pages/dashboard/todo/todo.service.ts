import { Injectable } from '@angular/core';

@Injectable()
export class TodoService {

  private _todoList = [
    { text: 'Check me out' },
    { text: 'You can add more here' },
  ];
  
  getTodoList() {
    return this._todoList;
  }
}
