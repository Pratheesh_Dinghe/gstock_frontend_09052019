import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ExpressService } from 'app/pages/express/express.service';
import { errorHandler } from './../../_helper/errorHandler';
import { LocalDataSource } from 'ng2-smart-table/lib/data-source/local/local.data-source';
import * as _ from 'lodash';
@Component({
  selector: 'az-express',
  templateUrl: './express.component.html',
  styleUrls: ['./express.component.scss'],
  providers: [ExpressService]
})
export class ExpressComponent implements OnInit {
  private expressForm: FormGroup;
  private expressList;
  private tableHead;
  private source = new LocalDataSource();
  private settings = {

  };
  constructor(
    private _fb: FormBuilder,
    private _expressService: ExpressService
  ) {
    this.formInit();
    this._expressService.getExpress().subscribe(
      express => {
        console.log(express);
        const colHead = {};
        const tableHeads = express[0].name.split('/');
        tableHeads.forEach(th => {
          colHead[th] = { 'title': th };
        });
        this.settings['columns'] = colHead;
        console.log(this.settings);
        this.expressList = express[0].value.map(v => { return _.zipObject(tableHeads, v.split(',')); });
        this.source.load(this.expressList);
      },
      err => errorHandler(err)
    );
  }

  ngOnInit() {
  }
  private formInit() {
    this.expressForm = this._fb.group({
      'name': [],
      'website': [],
      'contact': [],
      'fee': []
    });
  }
  private submit() {
    const form = this.expressForm.value;
    const option = 'name/website/contact/fee';
    const { name, website, contact, fee } = form;
    const value = [name, website, contact, fee];
    this._expressService.addExpress({
      name: option,
      value: value
    }).subscribe(
      onSuccess => console.log(onSuccess),
      err => errorHandler(err)
      );
  }
}
