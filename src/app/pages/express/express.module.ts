import { ExpressComponent } from './../express/express.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from './../../theme/directives/directives.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
export const routes = [
    { path: '', component: ExpressComponent },
];

@NgModule({
    imports: [
        CommonModule,
        DirectivesModule,
        RouterModule.forChild(routes),
        FormsModule,
        Ng2SmartTableModule,
        ReactiveFormsModule,
    ],
    declarations: [ExpressComponent]
})

export class ExpressModule { }
