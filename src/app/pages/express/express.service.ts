
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class ExpressService {
    private url = environment.apiUrl;
    constructor(private httpClient: HttpClient) {

    }

    public addExpress(body) {
        return this.httpClient.post(this.url + '/admin/attributes/express', body);
    }
    public getExpress() {
        return this.httpClient.get(this.url + '/admin/attributes/express');
    }
    public updateExpress(body) {
        return this.httpClient.put(this.url + '/admin/attributes/express', body);
    }

}
