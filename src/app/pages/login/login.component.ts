import { errorHandler } from '../../_helper/errorHandler';
import { AuthService } from '../../shared/auth.service';


import { Component, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AppState } from 'app/app.state';

@Component({
    selector: 'az-login',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {
    public router: Router;
    public form: FormGroup;


    constructor(
        router: Router,
        fb: FormBuilder,
        public _authService: AuthService,
        public _appState: AppState) {
        this.router = router;
        this.form = fb.group({
            email: ['', Validators.compose([Validators.required, emailValidator])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
        });


    }

    public onSubmit(values: Object): void {
        if (this.form.valid) {
            this._authService.login(this.form.value).subscribe(
                (onSuccess: any) => {
                    this._authService.setHeaders(onSuccess.token);
                    this._appState.notifyDataChanged('login', true);

                }, (err) => {
                    errorHandler(err);
                });
        }
    }
    public ngOnDestroy() {

    }
}

export function emailValidator(control: FormControl): { [key: string]: any } {
    const emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return { invalidEmail: true };
    }
}
