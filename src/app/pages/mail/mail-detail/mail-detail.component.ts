import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MailService } from '../mail.service';
import { Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'az-mail-detail',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './mail-detail.component.html'
})
export class MailDetailComponent implements OnInit {


    @Output() replyMessage = new EventEmitter();

    constructor(
        private service: MailService,
        private route: ActivatedRoute,
        private router: Router) {
    }

    ngOnInit() {
    }
}
