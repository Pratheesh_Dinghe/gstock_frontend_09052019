import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { MailService } from '../mail.service';
import { AppState } from "../../../app.state";

@Component({
    selector: 'az-mail-list',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './mail-list.component.html'
})
export class MailListComponent implements OnInit {
    public type: string;
    public isAllSelected: boolean;
    public searchText: string;

    constructor(
        private service: MailService,
        private route: ActivatedRoute,
        public router: Router,
        private state: AppState
    ) {

    }

    ngOnInit() {

    }

}
