import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MailService } from './mail.service';
import { AppState } from '../../app.state';

declare var gapi;
@Component({
    selector: 'az-mail',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './mail.component.html',
    styleUrls: ['./mail.component.scss'],
    providers: [MailService]
})
export class MailComponent {
    public content;
    public editorOptions = {
        key: '1recwzqA2oz==',
        placeholderText: 'Describe your product',
    };
    constructor(
        private _mailService: MailService,
        private route: ActivatedRoute,
        public router: Router,
        private state: AppState) {

    }
    ngAfterViewInit() {
        this.getTemplate();
    }
    getTemplate() {
        this._mailService.getEmailContent().subscribe(
            (s: any) => this.content = s.content,
            f => alert(f)
        );
    }
    sendTestEmail() {
        this._mailService.sendTestEmail().subscribe(
            (s: any) => ``,
            f => alert(f)
        );
    }
    updateEmailContent() {
        this._mailService.updateEmailContent({ content: this.content }).subscribe(
            (s: any) => ``,
            f => alert(f)
        );
    }
}

