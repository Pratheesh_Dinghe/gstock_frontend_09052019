
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class MailService {
    private url = environment.apiUrl;
    private headers;
    constructor(private httpClient: HttpClient) {


    }

    public getEmailContent() {
        return this.httpClient.get(`${this.url}/admin/email`);
    }
    public sendTestEmail() {
        return this.httpClient.get(`${this.url}/admin/email/test`);
    }
    public updateEmailContent(body) {
        return this.httpClient.put(`${this.url}/admin/email`, body);
    }

}
