import { environment } from '../../../../environments/environment';
import { errorHandler } from '../../../_helper/errorHandler';
import { Component, EventEmitter, Input, OnInit, Output, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewCell } from 'ng2-smart-table';

import { AuthService } from '../../../shared/auth.service';

@Component({
  selector: 'button-view',
  template: `
  <div style="position:relative; ">
    <img  style="max-width:60px;" [attr.src]="image">
  </div>
    `,
  providers: []
})

export class ImageColumnComponent implements ViewCell, OnInit {
  private image;
  @Input() value: string | number;
  @Input() rowData: any;
  constructor(
    private _authService: AuthService
  ) { }
  ngOnInit() {

    console.log('value', this.value);
    console.log('rowData', this.rowData);
    const {
      'brief.images': images,
      'product_id': productId
    } = this.rowData
    if (images && !images.length){return}
    let path= `${environment.apiUrl}/uploads/user/${this._authService.getUser()}/products/${productId}/${images[0]}`;
    console.log('Image Path', path);

    this.image = `${environment.apiUrl}/uploads/user/${this._authService.getUser()}/products/${productId}/${images[0]}`
  }
}

