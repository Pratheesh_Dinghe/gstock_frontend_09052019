import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import * as jsPDF from 'jspdf';
import { LocalDataSource } from 'ng2-smart-table';

import { environment } from '../../../../environments/environment';
import { errorHandler } from '../../../_helper/errorHandler';
import { OrderService } from '../order.service';
import { Router } from '@angular/router';
import { ImageColumnComponent } from './image-column.component';

@Component({
    selector: 'az-order-detail',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.scss']
})

export class OrderDetailComponent implements OnInit {
    private status;
    private shipmentForm: FormGroup;
    private orderStatus = [];
    private source = new LocalDataSource();
    private order;
    private products;
    private merchant;
    private summary = {
    };

    private settings = {
        actions: {
            delete: false,
            edit: false,
            add: false
        },
        columns: {
            'name': {
                title: 'Product Name'
            },
            'sku': {
                title: 'SKU'
            },
            'option_name': {
                title: 'Spec Name'
            },
            'option_value': {
                title: 'Value'
            },
            'price': {
                title: 'Usual Price'
            },
            'discount_price': {
                title: 'Discount Price'
            },
            'qty': {
                title: 'qty'
            },
            'subtotal': {
                title: 'Subtotal'
            },
            'commission': {
                title: "commission"
            },
            'reward_pts': {
                title: 'Reward Points'
            }
        }
    };
    constructor(
        public _orderService: OrderService,
        private activatedRoute: ActivatedRoute,
        private _fb: FormBuilder,
        private _route: Router
    ) {
        this.orderStatus = this._orderService.getOrderStatus();
        this.shipmentForm = this._fb.group(
            {
                'agent': ['',],
                'first_name': ['',],
                'fromAddress': ['',],
                'fromBld': ['',],
                'fromPostcode': [''],
                'fromName': ['',],
                'from_contact_no': [],
                'contact_no': ['',],
                'fromEmail': ['',],
                'address': ['',],
                'unit_no': ['',],
                'postal_code': ['',],
                'recepient': ['',],
                'toEmail': ['',],
            }
        );
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            if (!params) { return; }
            this._orderService.getOrderDetail(params['orderid']).subscribe(
                (data: any) => {

                    this.order = data;
                    const {
                        buyer_id,
                        delivery,
                        status,
                        total,
                        commission_status
                    } = this.order;
                    this.shipmentForm.patchValue({
                        ...delivery || {},
                        ...buyer_id.profile || {},
                        ...buyer_id.contact || {}
                    });
                    this.status = status;
                    const detailArray = this.linearizeNestedArray(this.order);
                    this.summary = {
                        totalItem: detailArray.length,
                        totalShipment: delivery.shipping_fee,
                        totalCommission: total.commission.toFixed(2),
                        totalRewardPts: total.reward_pts.toFixed(2),
                        totalProductAmount: total.store_bp,
                        totalDiscount: total.store_bp - total.store_ap,
                        totalPayable: total.store_ap + delivery.shipping_fee,
                        commissionStatus: commission_status
                    }
                    this.products = detailArray.map(r => {
                        const {
                            variant: v,
                            product: p,
                        } = r;
                        const discountPrice = p.brief.discount ? (v.price || p.brief.price) * (100 - p.pricing.discount_rate) / 100 : p.brief.price;
                        return {
                            ...p,
                            ...v,
                            product_id: p._id,
                            variant_id: v._id,
                            name: p.brief.name,
                            price: v.price || p.brief.price,
                            discount_price: discountPrice,
                            qty: v.order_qty,
                            subtotal: discountPrice * v.order_qty,
                        };
                    });
                    this.source.load(this.products);
                },
                err => {
                    throw (err);
                }
            );
        });
    }
    private generateInvoice() {
        const doc = new jsPDF();
        doc.text(20, 20, 'From ' + this.order.merchant_id.profile.first_name);
        doc.text(20, 30, 'No.');
        doc.text(150, 20, 'Sales Receipt');
        doc.text(20, 50, 'SOLD TO:');
        doc.text(150, 50, 'SHIP TO');
        doc.rect(30, 90, 150, 90);
        doc.line(30, 100, 180, 100);
        doc.line(30 + 37.5, 90, 30 + 37.5, 180);
        doc.line(30 + 37.5 * 2, 90, 30 + 37.5 * 2, 180);
        doc.line(30 + 37.5 * 3, 90, 30 + 37.5 * 3, 180);
        doc.text(33, 97, 'Description');
        doc.text(33 + 37.5 * 1, 97, 'Quantity');
        doc.text(33 + 37.5 * 2, 97, 'Unit Price');
        doc.text(33 + 37.5 * 3, 97, 'Amount');
        doc.text(150, 240, 'Total Amount');
        this.products.forEach(
            (p, i) => {
                console.log(p);
                doc.text(33, 97 + 10 * (i + 1), p.name);
                doc.text(33 + 37.5 * 1, 97 + 10 * (i + 1), p.qty + '');
                doc.text(33 + 37.5 * 2, 97 + 10 * (i + 1), p.price + '');
                doc.text(33 + 37.5 * 3, 97 + 10 * (i + 1), p.qty * p.price + '');
                doc.text(150, 240, 'Total Amount');
                doc.line(30, 100 + 10 * (i + 1), 180, 100 + 10 * (i + 1));
            }
        );

        // Save the PDF
        doc.save('Test.pdf');
    }

    private linearizeNestedArray(order) {
        const display = [];
        const products = order.products;
        products.forEach(p => {
            let img = '../assets/images/default-image.jpg';
            if (undefined !== p.product.brief.images && 0 !== p.product.brief.images.length) {
                img = `${environment.apiUrl}/uploads/user/${order.merchant_id._id}/products/${p.product._id}/${p.product.brief.images[0]}`;
            }
            return p.variants.forEach(variant => {
                const { order_qty } = variant;
                const toBePushed = {
                    ...p,
                    order_qty,
                    img,
                    variant
                };
                return display.push(toBePushed);
            });
        });
        return display;
    }

    private updateStatus() {
        if (!confirm('Are you sure you want to change order status? This action is irreversible.')) { return; }
        this._orderService.updateOrder({
            '_id': this.order._id,
            'status': this.status
        }).subscribe((message) => {
            alert(message);
            return;
        },
            err => { return errorHandler(err); });
    }
    private selectProduct(e) {
        this._route.navigate(['/pages/products/product-detail', e.data.product_id]);
    }
    private deleteOrder() {
        if (!confirm('I surely hope you know what you are doing. Proceed?')) { return; }
        this._orderService.deleteOrder({ order: this.order['_id'] }).subscribe(
            (s) => console.log(s),
            (e) => console.log(e)
        );
    }
}
