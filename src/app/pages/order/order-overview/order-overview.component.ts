import { errorHandler } from '../../../_helper/errorHandler';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { OrderService } from '../order.service';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import * as flatten from 'flatten-obj';
import { DELIVERY_STATUS } from '../../../_global/status';
@Component({
    selector: 'az-order-overview',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './order-overview.component.html',
    styleUrls: ['./order-overview.component.scss'],
})

export class OrderOverviewComponent implements OnInit {
    private source = new LocalDataSource();
    private selected = [];
    private data = [];
    private settings = {
        actions: {
            delete: false,
            edit: false,
            add: false
        },
        columns: {
            _id: {
                title: 'Order ID'
            },
            'buyer_id.profile.first_name': {
                title: 'Buyer'
            },
            'merchant_id.profile.first_name': {
                title: 'Seller'
            },
            'createdAt': {
                title: 'Date',
                valuePrepareFunction: (date) => {
                    const raw = new Date(date);
                    const formatted = new DatePipe('en-EN').transform(raw, 'dd-MM-yyyy HH:mm');
                    return formatted;
                }
            },
            'total.store_ap': {
                title: 'Amount'
            },
            'status': {
                title: 'Status',
                // valuePrepareFunction: (status) => DELIVERY_STATUS[status]
            }
        }
    };

    constructor(
        private router: Router,
        public _orderService: OrderService
    ) {
        _orderService.getOrders().subscribe(
            (data: Array<any>) => {
                const flattened = data.map(d => flatten()(d));
                console.log(flattened);
                this.source.load(flattened);
            },
            err => errorHandler(err)
        );



    }
    ngOnInit(): void {

    }

    private selectOrder(event): void {
        this.router.navigate(['pages/order/order-detail/' + event.data._id]);
    }

    // onSelect({ selected }) {
    //     console.log('Select Event', selected, this.selected);

    //     this.selected.splice(0, this.selected.length);
    //     this.selected.push(...selected);
    // }
    // onActivate(event) {
    //     console.log('Activate Event', event);
    // }


}
