import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { OrderService } from './order.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { OrderOverviewComponent } from './order-overview/order-overview.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ImageColumnComponent } from './order-detail/image-column.component';
export const routes = [
  { path: '', redirectTo: 'order-overview', pathMatch: 'full' },
  { path: 'order-overview', component: OrderOverviewComponent, data: { breadcrumb: 'Order Overview' } },
  { path: 'order-detail/:orderid', component: OrderDetailComponent, data: { breadcrumb: 'Order Detail' } }

];

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    AgmCoreModule,
    RouterModule.forChild(routes),
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [OrderOverviewComponent, OrderDetailComponent],
  providers: [OrderService]
})

export class OrderModule { }
