
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class OrderService {
  private url = environment.apiUrl;
  constructor(private httpClient: HttpClient) {

  }

  public getOrderDetail(orderID) {
    return this.httpClient.get(`${this.url}/admin/order?order_id=${orderID}`);
  }

  public getOrderSummary(orderId) {
    const orderSummary = {
      seller: 'Tony-Tony',
      buyer: 'David',
      orderDate: '23/09/2017 23:23:44',
      orderStatus: 'as'
    };
    return orderSummary;
  }

  public generateInvoice() {
    return this.httpClient.get(this.url + '/admin/invoice', { responseType: 'blob' });
  }

  public searchUsersByUserId(body) {
    return this.httpClient.post(this.url + '/admin/searchusersbyuserid', body);

  }

  public updateOrder(body) {
    return this.httpClient.put(`${this.url}/admin/order/status?order_id=${body._id}`, { status: body.status });
  }
  public getOrders() {
    return this.httpClient.get(this.url + '/admin/order');
  }
  public getComplaint() {
    return this.httpClient.get(this.url + '/admin/order/complaint');
  }
  public getOrderStatus() {

    return [
      {
        name: 'Awaiting Delivery',
        value: 'AD'
      },
      {
        name: 'Delivering',
        value: 'DG'
      },
      {
        name: 'Goods Received',
        value: 'GR'
      },
    ];
  }
  public deleteOrder(body) {
    return this.httpClient.post(`${this.url}/admin/order/bulk/delete`, body);
  }
}
