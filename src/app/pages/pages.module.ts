import { UtilComponentsModule } from '../theme/util-components/util.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarConfigInterface, PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
import { ToastrModule } from 'ngx-toastr';

import { BackTopComponent } from '../theme/components/back-top/back-top.component';
import { BreadcrumbComponent } from '../theme/components/breadcrumb/breadcrumb.component';
import { MenuComponent } from '../theme/components/menu/menu.component';
import { MessagesComponent } from '../theme/components/messages/messages.component';
import { NavbarComponent } from '../theme/components/navbar/navbar.component';
import { SidebarComponent } from '../theme/components/sidebar/sidebar.component';
import { DirectivesModule } from '../theme/directives/directives.module';
import { PipesModule } from '../theme/pipes/pipes.module';
import { PagesComponent } from './pages.component';
import { routing } from './pages.routing';
import { PagesService } from './pages.service';
import { CategoryService } from 'app/pages/products/cate/cate.service';





@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    ToastrModule.forRoot(),
    DirectivesModule,
    PipesModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    UtilComponentsModule
  ],
  declarations: [
    PagesComponent,
    MenuComponent,
    SidebarComponent,
    NavbarComponent,
    MessagesComponent,
    BreadcrumbComponent,
    BackTopComponent,

  ],
  providers: [PagesService, CategoryService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})
export class PagesModule { }
