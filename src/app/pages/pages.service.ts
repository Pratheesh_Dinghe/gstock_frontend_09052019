import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

import { environment } from '../../environments/environment';

@Injectable()
export class PagesService {
    private url = environment.apiUrl;
    constructor(private http: Http) {

    }
    public createUser(body) {
        return this.http.post(this.url + '/admin/createusers', body, );
    }

    public updateUsers(body) {
        return this.http.post(this.url + '/admin/user/updateusers', body, );
    }
}
