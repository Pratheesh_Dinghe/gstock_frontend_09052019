import { Component, ViewEncapsulation } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { errorHandler } from '../../../_helper/errorHandler';
import { CategoryService } from './cate.service';


@Component({
  selector: 'az-cate',
  templateUrl: './cate.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./cate.component.scss'],
})
export class CateComponent {
  private triplePanel = [[], [], []];
  private rawCatList = [];
  private selected = ['', '', ''];
  private attributeSelected;
  private rowSelected;
  private source = new LocalDataSource();
  private settingsAttribute = {
    actions: {
      edit: false
    },
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    columns: {
      name: {
        title: 'Attribute Name'
      }
    },
  };
  private settings = {
    actions: {
      delete: false,
    },
    add: {
      confirmCreate: true,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      name: {
        title: 'Cateogry Name'
      },
      commission: {
        title: 'commission'
      }
    },
  };
  private displaySet = {
    attribute: [],
    variant: [],
  };
  private total = {
    attribute: [],
    variant: [],
  };
  private totalPages = {
    attribute: 0,
    variant: 0,
  };
  private assignment = {
    variant: [],
    attribute: []
  };
  constructor(
    public _categoryService: CategoryService,
    // public _cd: ChangeDetectorRef
  ) {
    this.loadTable((data) => {
      this.rawCatList = data;
      this.select('', -1);
    });
    this._categoryService.getProductAttribute().subscribe(
      (pa: any) => {
        this.displaySet = this.arrangeVariantAndAttributes(pa);
        const { attribute, variant } = this.displaySet;
        this.total.attribute = attribute;
        this.total.variant = variant;
        this.totalPages.attribute = Math.ceil(attribute.length / 12);
        this.totalPages.variant = Math.ceil(variant.length / 12);
        this.toAPage(1);
        this.toVPage(1);
        this.source.load(pa);
      },
      e => errorHandler(e)
    );
  }

  ngAfterViewInit() {

  }
  /**
   * Attribute Management
   */
  private copyValue;
  private toBeRemoved = [];
  private createAttribute(e) {
    this._categoryService.createProductAttribute({
      name: e.newData['name'],
      value: []
    }).subscribe(
      s => {
        e.confirm.resolve();
        alert('Attribute created');
      }, err => errorHandler(err)
    );
  }
  private removeAttribute(e) {
    this._categoryService.removeProductAttribute(e.data['_id']).subscribe(
      s => {
        e.confirm.resolve();
        alert('Attribute deleted');
      },
      err => errorHandler(err)
    );
  }
  private editAttribute(a) {
    this.attributeSelected = a;
    console.log(this.attributeSelected.data);
    this.toBeRemoved = [];
    this.copyValue = undefined;
    this.copyValue = { ...this.copyValue, ...a.data };
    // console.log(this.copyValue.variant);
    // $('[name=\'my-checkbox\']').bootstrapSwitch({
    //   state: this.copyValue.variant
    // });
    $('#attributeModal').modal('show');
  }
  private updateAttribute() {
    this.toBeRemoved.forEach((r, i) => {
      if (r) {
        this.copyValue.value[i] = false;
      }
    });
    this.copyValue.value = this.copyValue.value.filter(v => v);
    const { _id: attribute_id, value, variant } = this.copyValue;
    this._categoryService.updateProductAttribute({
      attribute_id,
      value,
      variant
    }).subscribe(
      s => {
        this.toBeRemoved = [];
        this.attributeSelected.data.value = this.copyValue.value;
        alert(s);
      },
      e => errorHandler(e)
    );
  }

  /**
   *
   * Attribute Asssigment
   */
  arrangeVariantAndAttributes(array) {
    const variant = [];
    const attribute = [];
    array.forEach(e => e.variant ? variant.push(e) : attribute.push(e));
    return { variant, attribute };
  }
  private openAttributesForAssignmentModal(rowSelected) {
    this.rowSelected = rowSelected;
    // this.assignment.clone = rowSelected.data.attributes || [];
    this.assignment = this.arrangeVariantAndAttributes(rowSelected.data.attributes || []);
    $('#attributeSetModal').modal('show');
  }
  private assignAttribute(selected, a) {
    const { variant: isVariant } = a;
    const { variant, attribute } = this.assignment;
    if ([...variant, ...attribute].find(v => v.name === selected.innerText)) { return; }
    if (isVariant) {
      if (variant.length >= 2) { return alert('You cant assign more than 2 variants'); }
      // this.clonedAssignment.clone.push(a);
      this.assignment.variant.push(a);
    } else {
      if (attribute.length >= 4) { return alert('You cant assign more than 4 attributes'); }
      // this.clonedAssignment.clone.push(a);
      this.assignment.attribute.push(a);
    }
  }
  private saveAssignedAttribute() {
    const { variant, attribute } = this.assignment;
    const body = {
      path: `,${this.selected.join(',')},`,
      attributes: Array.from(new Set([...variant, ...attribute].map(a => a._id)))
    };
    this._categoryService.assignAttributeToCategory(body).subscribe(
      s => this.rowSelected.data.attributes = [...variant, ...attribute],
      e => errorHandler(e)
    );
  }
  private removeAssignedVariant(i) {
    this.assignment.variant.splice(i, 1);
  }
  private removeAssignedAttribute(i) {
    this.assignment.attribute.splice(i, 1);
  }
  private toVPage(x) {
    const start = (x - 1) * 12;
    this.displaySet.variant = this.total.variant.slice(start, start + 12);
    // this._cd.detectChanges();
  }
  private toAPage(x) {
    const start = (x - 1) * 12;
    this.displaySet.attribute = this.total.attribute.slice(start, start + 12);
  }

  /**
   *
   * Category & Commission Management
   */
  private loadTable(cb?) {
    /* Load source and perform a callback once loaded */
    return this._categoryService.getAllCategory().subscribe(data => {
      cb(data);
    });
  }
  private select(event, level, editing?) {
    /* Open up subcategory branch by name and leve
     * Refresh selected path and panel
     */
    if (!editing && 2 === level) {
      this.openAttributesForAssignmentModal(event);
      // return;
    }
    const parentCategoryName = '' === event ? '' : (event.newData || event.data).name;
    const body = {
      original: this.rawCatList,
      selected: this.selected,
      parentCategoryName: parentCategoryName,
      pathAccumulator: []
    };
    const { pathAccumulator, path } = this.expand(body, level);
    pathAccumulator.forEach((e, i) => {
      this.triplePanel[i + ('' === parentCategoryName ? 0 : level + 1)] = e;
    });
    return path;
  }
  private expand(body, level) {
    /* Recursively open up subcategory branch to last level
     * Return subcategory array and the path
     */
    body.selected[level] = body.parentCategoryName;
    // When last level, return the paths.
    const path = body.selected.slice(0, level + 1);
    const childCategories = CategoryService.subCat(body.original, path.join(','), level + 1);
    // expand the parent category becomse first child category
    if (childCategories && childCategories.length) {
      body.parentCategoryName = childCategories[0].name;
      body.pathAccumulator.push(childCategories);
    }
    if (('' === name && -1 === level) || (1 <= level)) {
      return {
        pathAccumulator: body.pathAccumulator,
        path: path
      };
    }

    return this.expand(body, level + 1);
  }
  private onCreateConfirm(event, level) {
    const newName = event.newData.name;
    if (!level) {
      return this._categoryService.postCreateMainCat({ name: `,${newName},` })
        .subscribe((message) => {
          alert(message);
          this.loadTable((data) => {
            this.rawCatList = data;
            this.select(event, level);
            event.confirm.resolve();
          });
          event.confirm.resolve();
        },
          (err) => errorHandler(err));
    }
    const path = this.selected.slice(0, level);
    const parentPath = `,${path.join(',')},`;
    const body = {
      parent: parentPath,
      new_path: newName,
    };
    return this._categoryService.postAddSubCat(body)
      .subscribe((message) => {
        alert(message);
        this.loadTable((data) => {
          this.rawCatList = data;
          this.select(event, level);
          event.confirm.resolve();
        });
        event.confirm.resolve();
      },
        (err) => {
          return errorHandler(err);
        });
  }
  private onEditConfirm(event, level) {
    const path = this.select(event, level, true);
    if (event.data.name !== event.newData.name) {
      const oldPath = path.slice(0, level);
      const newPath = path.slice(0, level);
      oldPath.push(event.data.name);
      newPath.push(event.newData.name);
      const body = {
        old: `,${oldPath.join(',')},`,
        new: `,${newPath.join(',')},`,
      };
      this._categoryService.putUpdateSubCat(body).subscribe(
        (message) => {
          this.loadTable((data) => {
            this.rawCatList = data;
            this.select(event, level);
            event.confirm.resolve();
          });
          event.confirm.resolve();
        },
        (err) => errorHandler(err));
    }
    if (event.data.commission !== event.newData.commission) {
      const body = {
        path: path.join(','),
        commission: event.newData.commission
      };
      this._categoryService.putUpdateCommission(body).subscribe(
        (message) => {
          alert(message);
          this.loadTable((data) => {
            this.rawCatList = data;
            this.select(event, level);
            event.confirm.resolve();
          });
          event.confirm.resolve();
        },
        (err) => errorHandler(err));
    }
  }
}
