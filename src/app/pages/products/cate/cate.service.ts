import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { AuthService } from '../../../shared/auth.service';




@Injectable()
export class CategoryService {
    public static subCat(original: Array<any>, parent: string, offset: number) {
        /* Retrive data.path array and transform to array of category by level
         * Root categories array start at level -1
         */
        const myReg = new RegExp(`^,${parent},.+?,`, 'i');
        const subCategory = original
            .filter(filter => parent ? myReg.exec(filter.path) : true)
            /* Transfrom into array e.g. => ['','Food','Baby','Milk'] =>['Food','Baby','Milk']
             * Slice to get sub category ['Food','Baby','Milk'].slice(1+ offset) => Food
             */
            .map(s => s.path.split(','))
            .map(pathArray => pathArray.slice(1 + offset)[0]);
        const unique = Array.from(new Set(subCategory)).map((e: string) => {
            const match = original.find((self) => {
                return self.path === (parent !== '' ? `,${parent},${e},` : `,${e},`);
            });
            return {
                name: e,
                commission: match ? match.commission : '没有',
                attributes: match ? match.attributes : [],
            };
        });
        return unique;
    }
    private url = environment.apiUrl;
    private headers;

    constructor(
        private http: HttpClient,
        private _authService: AuthService
    ) {
        this.headers = _authService.getHeaders();
    }

    postCreateMainCat(body) {
        return this.http.post(this.url + '/admin/category', body);
    }
    getAllCategory(path?) {
        if (path) { return this.http.get(`${this.url}/admin/category/all?path=${path}`); }
        return this.http.get(this.url + '/admin/category/all');
    }
    putUpdateSubCat(body) {
        return this.http.put(this.url + '/admin/category', body);
    }
    deleteRemoveSubCat(body) {
        return this.http.post(this.url + '/admin/category/delete', body);
    }
    postAddSubCat(body) {
        return this.http.post(this.url + '/admin/category/append', body);
    }
    putUpdateCommission(body) {
        return this.http.put(this.url + '/admin/category/commission', body);
    }

    getProductAttribute() {
        return this.http.get(`${this.url}/admin/category/attribute`);
    }
    updateProductAttribute(body) {
        return this.http.put(`${this.url}/admin/category/attribute`, body);
    }
    createProductAttribute(body) {
        return this.http.post(`${this.url}/admin/category/attribute`, body);
    }
    removeProductAttribute(attributeId) {
        return this.http.delete(`${this.url}/admin/category/attribute?attribute_id=${attributeId}`);
    }

    assignAttributeToCategory(body) {
        return this.http.post(`${this.url}/admin/category/attribute/assign`, body);
    }

}
