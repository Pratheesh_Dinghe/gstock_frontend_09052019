import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'az-image-modal',
  templateUrl: './image-modal.component.html',
  styleUrls: ['./image-modal.component.scss']
})
export class ImageModalComponent implements OnInit {
  @Input('imagesPool') private imagesPool = [];

  @Input('mode') private mode: string;

  @Input('currentImageNumber') private currentImageNumber: number;

  @Output('tmpI') done: EventEmitter<any> = new EventEmitter();

  public tmpImages = [];
  constructor() { }

  ngOnInit() {
  }

  private removeImage(imagesArray, index): void {
    imagesArray.splice(index, 1);
  }
  private selectImage(i) {
    if (this.tmpImages[i] !== undefined) {
      this.tmpImages[i] = undefined;
    } else {
      console.log(this.tmpImages.filter(t => {
        return t !== undefined ? true : false;
      }).length);
      if (this.tmpImages.filter(t => {
        return t !== undefined ? true : false;
      }).length > 3) {
        return alert('No more than 4 images can be uploaded!');
      }
      this.tmpImages[i] = i;
    }
    console.log(this.tmpImages);
  }
  private doneSelect() {
    const selected = this.tmpImages
      .filter(t => {
        return t !== undefined ? true : false;
      })
      .map(i => { return this.imagesPool[i]; });
    this.done.emit(selected);
    return;
  }
}
