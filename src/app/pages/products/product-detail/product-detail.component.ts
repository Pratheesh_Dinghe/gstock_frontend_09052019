import { errorHandler } from '../../../_helper/errorHandler';
import { CategoryService } from '../cate/cate.service';
import { DatePipe, Location, DecimalPipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { Observable } from 'rxjs/Observable';
import { ProductService } from '../products.service';

import { NewProductService } from './product-detail.service';
import { ImageModalComponent } from 'app/pages/products/product-detail/image-modal/image-modal.component';
import { environment } from 'environments/environment';


@Component({
    selector: 'az-product-detail',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.scss'],
    providers: [NewProductService]
})

export class ProductNewComponent implements OnInit {
    @ViewChild('imageModal') imageModal: ImageModalComponent;
    private product: any = {
    };
    private category = '';
    private images;
    private source: LocalDataSource;
    private settings = {
        actions: false,
        columns: {
            option_name: {
                title: 'Option',
                editable: false
            },
            option_value: {
                title: 'Value',
                editable: false
            },
            price: {
                title: 'Selling Price',
            },
            // discount_price: {
            //     title: 'Discount Price',
            // },
            stock: {
                title: 'Qty',
            },
            sku: {
                title: 'SKU',
                editable: true
            }
        }
    };
    private reasons = [];
    private improvement = [];
    private apiUrl = `${environment.apiUrl}/uploads/`;
    constructor(
        private _productService: ProductService,
        private _categoryService: CategoryService,
        private _newProductService: NewProductService,
        private _location: Location,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private changeDetectorRef: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private _activatedRoute: ActivatedRoute
    ) {
        this.source = new LocalDataSource();
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe(
            params => {
                if (!params.id) { return; }
                this._productService.getSingleProduct(params.id).subscribe(
                    async (data: any) => {
                        const { product, variants } = data;
                        this.product = this.presentProduct(product);
                        console.log('product (after)', this.product);
                        if (variants.length) { this.formatPresentation(variants); }
                    },
                    err => {
                        throw (err);
                    }
                );
            });
    }
    private seprateVariantsFromAttributes(attributesArray: Array<any>) {
        const variants = [], attributes = [];
        if (attributesArray && attributesArray.length) {
            attributesArray.forEach(a => a.variant ? variants.push(a) : attributes.push(a));
        }
        return {
            variants: variants,
            attributes: attributes
        };
    }
    private presentProduct(product) {
        console.log('product (before)', product);
        const {
            status,
            discount,
            reward_pts: rewardPts,
            category_id: { _id: categoryId, path, commission, attributes: attributesArray },
            pricing: { discount_price: discountPrice, discount_rate: discountRate },
            detail: { product_brand: brand, sku, long_description: longDescription, barcode },
            brief: { stock, price, name, images, short_description: shortDescription },
            merchant_id: merchant,
            merchant_id: {
                _id: merchantId,
                credential: { email: merchantEmail },
                profile: { first_name: merchantName },
                store: { name: storeName },
                commission: specialCommission
            },
            _id: productId
        } = product;
        const src = images.map(i => `${environment.apiUrl}/uploads/user/${merchantId}/products/${productId}/${i}`);
        if (!src.length) { this.attention('Images'); }
        const specialRate = specialCommission.find(e => e.category_id === categoryId);
        const { attributes, variants } = this.seprateVariantsFromAttributes(attributesArray);
        return {
            attributes,
            variants,
            productId,
            rewardPts,
            discountRate,
            discountPrice,
            category: path.split(',').join('/'),
            categoryCommission: commission,
            images: src,
            productName: name || this.attention('Name'),
            merchant: merchant || this.attention('Merchant'),
            merchantId,
            storeName: storeName || this.attention('Store name'),
            merchantEmail,
            merchantName,
            longDescription: longDescription || this.improve('Short product description'),
            shortDescription: shortDescription,
            stock: stock || this.attention('Stock'),
            price: price || this.attention('Price'),
            sku: sku || this.improve('SKU'),
            brand: brand || this.improve('brand'),
            status,
            barcode,
            special: specialCommission.length,
            specialRate: specialRate ? specialRate.rate : specialRate
        };
    }
    private formatPresentation(variants) {
        if (variants.length) { this.source.load(variants); }
    }
    private formInit() {
        const generalSetting = [{
            formGroupDisplayName: 'Detail',
            formGroupName: 'detail',
            formControls: [
                {
                    displayName: 'Price',
                    formControlName: 'price',
                    type: 'text',
                    set: []
                },
                {
                    displayName: 'Discount Price',
                    formControlName: 'discount_price',
                    type: 'text',
                    set: []
                },
                {
                    displayName: 'Stock',
                    formControlName: 'stock',
                    type: 'text',
                    set: []
                },
                {
                    displayName: 'SKU',
                    formControlName: 'sku',
                    type: 'text',
                    set: []
                }]
        }];
    }
    private changeProductStatus(status) {
        return this._productService.changeProductStatus(status, this.product.productId).subscribe(
            (result) => {
                alert(result.message);
                this.product.status = status;
            },
            (err) => {
                errorHandler(err);
            }
        );

    }
    attention(item) {
        this.reasons.push(`${item} is missing or quantity should not be 0`);
        return 'Information required!';
    }
    improve(item) {
        this.improvement.push(`Should provide ${item}`);
        return 'Improvement needed!';
    }
    select() {
    }
    update() {
        const { brand, productName, sku, barcode, shortDescription } = this.product;
        const body = {
            detail: {
                product_brand: brand,
                sku: sku,
                barcode: barcode
            },
            brief: {
                name: productName,
                short_description: shortDescription
            }
        };
        const { productId } = this.product;
        this._productService.updateProduct(productId, body).subscribe(
            s => alert(s),
            e => errorHandler(e)
        );
    }
    deleteProduct() {
        if (!confirm('**Warning** You are performing an irreversible action. Are you sure to proceed?')) { return; }
        const { productId } = this.product;
        this._productService.deleteProduct(productId).subscribe(
            s => {
                alert(s);
                this._location.back();
            },
            e => errorHandler(e)
        );
    }

    afterViewInit() {
        throw (new Error(''));
    }
}

