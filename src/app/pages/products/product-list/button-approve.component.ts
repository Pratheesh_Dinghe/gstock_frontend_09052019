import { environment } from '../../../../environments/environment';
import { errorHandler } from '../../../_helper/errorHandler';
import { Renderer2, Component, ViewChild, ElementRef, EventEmitter, Input, OnInit, Output, ChangeDetectorRef } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    selector: 'button-approve',
    template: `
  <div style="position:relative; text-align:center">
  <button #status type="button" class="btn-rounded" style="padding: 0px 20px;">
        {{rowData.status === undefined ? 'Pending Review' : rowData.status}}
    </button>
  </div>
    `,
})
export class ButtonApproveComponent implements ViewCell {
    @Input() value;
    @Input() rowData: any;
    @ViewChild('status', { read: ElementRef }) status: ElementRef;
    constructor(
        private renderer: Renderer2
    ) {
    }
    ngOnInit() {
        switch (this.rowData.status) {
            case 'Approved': {
                this.renderer.addClass(this.status.nativeElement, 'btn-success');
                break;
            }
            case 'Rejected': {
                this.renderer.addClass(this.status.nativeElement, 'btn-danger');
                break;
            }
            case 'Pending': {
                this.renderer.addClass(this.status.nativeElement, 'btn-info');
                break;
            }
        }
    }
}

