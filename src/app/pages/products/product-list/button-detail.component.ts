import { environment } from '../../../../environments/environment';
import { errorHandler } from '../../../_helper/errorHandler';
import { Component, EventEmitter, Input, OnInit, Output, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewCell } from 'ng2-smart-table';
import { Router } from '@angular/router';
@Component({
    selector: 'button-detail',
    template: `
  <button (click)="selectProduct()">View</button>
    `,
})
export class ButtonDetailComponent implements ViewCell, OnInit {
    @Input() value: string | number;
    @Input() rowData: any;
    constructor(
        private sanitizer: DomSanitizer,
        private changeDetectorRef: ChangeDetectorRef,
        private _router: Router
    ) { }
    ngOnInit() {
    }
    private selectProduct(): void {
        this._router.navigate(['pages/products/product-detail/' + this.rowData.id]);
    }

}

