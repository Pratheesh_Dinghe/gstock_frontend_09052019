import { environment } from '../../../../environments/environment';
import { errorHandler } from '../../../_helper/errorHandler';
import { Component, EventEmitter, Input, OnInit, Output, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ViewCell } from 'ng2-smart-table';


@Component({
  selector: 'button-view',
  template: `
  <div style="position:relative" (mouseover)="startTimer()"  (mouseleave)="onMouseleave()">
    <button>View image</button>
    <img *ngIf="hovered===1" style="position:absolute;max-width:80px;" [attr.src]="image">
  </div>
    `,
})
export class ButtonViewComponent implements ViewCell, OnInit {
  private image;
  private hovered;
  private timer;
  @Input() value: string | number;
  @Input() rowData: any;
  @Output() showImage: EventEmitter<any> = new EventEmitter();
  constructor(
    private sanitizer: DomSanitizer,
    private changeDetectorRef: ChangeDetectorRef
  ) { }
  ngOnInit() {

  }

  private startTimer() {
    this.hovered = 1;
    return this.timer = setTimeout(() => this.show(), 1000);
  }
  private show() {
    if (this.rowData['brief.images'] === undefined) { return; }
    if (this.image || 0 === this.rowData['brief.images'].length) { return; }
    const productId = this.rowData._id;
    const merchantId = this.rowData['merchant_id._id'];
    setTimeout(() => {
      this.showImage.emit(this.rowData);
      this.image = environment.apiUrl + '/uploads/user/' + merchantId + '/products/' + productId + '/' + this.rowData['brief.images'][0];
      this.changeDetectorRef.detectChanges();
    }, 1000);
  }
  private onMouseleave() {
    clearTimeout(this.timer);
    this.hovered = -1;
    return;
  }
}

