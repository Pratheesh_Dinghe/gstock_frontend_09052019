import { errorHandler } from '../../../_helper/errorHandler';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, ViewChild, ElementRef, EventEmitter, Input, OnInit, Output, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import * as flatten from 'flatten-obj';
import { LocalDataSource } from 'ng2-smart-table';

import { ProductService } from '../products.service';
import { ButtonViewComponent } from './button-view.component';
import { ButtonApproveComponent } from './button-approve.component';
import { DatePipe } from '@angular/common';
import { ButtonDetailComponent } from 'app/pages/products/product-list/button-detail.component';


@Component({
    selector: 'az-product-list',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss'],
    entryComponents: [ButtonViewComponent, ButtonApproveComponent, ButtonDetailComponent]
})
export class ProductListComponent implements OnInit {
    @ViewChild('myModal') myModal: ElementRef;
    private searchForm: FormGroup;
    private query = '';
    private source = new LocalDataSource();
    private origin = [];
    // private statusChangeEmitter: EventEmitter<string> = new EventEmitter();
    private productIdPendingApproval = [];
    private settings = {
        selectMode: 'multi',
        actions: false,
        columns: {
            'brief.name': {
                title: 'Product Name'
            },
            'merchant_id.store.name': {
                title: 'Merchant'
            },
            // 'carousel': {
            //     title: 'Picture',
            //     type: 'custom',
            //     renderComponent: ButtonViewComponent,
            //     onComponentInitFunction: (instance) => {
            //         instance.showImage.subscribe(row => {
            //             console.log(row);
            //         });
            //     }
            // },
            'status': {
                title: 'Status',
                type: 'custom',
                renderComponent: ButtonApproveComponent
            },
            'is_active': {
                title: 'Is activated?'
            },
            'createdAt': {
                title: 'Create Date',
                valuePrepareFunction: (date) => {
                    if (!date) { return; }
                    const raw = new Date(date);
                    const formatted = new DatePipe('en-EN').transform(raw, 'dd-MM-yyyy HH:mm');
                    return formatted;

                }
            },
            'updatedAt': {
                title: 'Update Date',
                valuePrepareFunction: (date) => {
                    if (!date) { return; }
                    const raw = new Date(date);
                    const formatted = new DatePipe('en-EN').transform(raw, 'dd-MM-yyyy HH:mm');
                    return formatted;
                }
            },
            'viewDetail': {
                title: 'Detail',
                type: 'custom',
                renderComponent: ButtonDetailComponent,
            }
        }
    };

    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _router: Router,
        private _productService: ProductService,
        private fb: FormBuilder
    ) {
    }
    ngOnInit() {
        this.query = 'name=';
        this.formInit();
        this.toPage(1);
    }
    private formInit() {
        this.searchForm = this.fb.group({
            'name': [],
            'store_name': [],
        });
    }

    private changeProductStatus(status) {
        if (!this.productIdPendingApproval.length) { return; }
        return this._productService.changeProductStatus(status, this.productIdPendingApproval).subscribe(
            (result) => this.toPage(1),
            (err) => errorHandler(err)
        );
    }
    private totalPages;
    private searchProduct() {
        const form = this.searchForm.value;
        this.query = Object.keys(form)
            .map(field => form[field] ? `${field}=${form[field].trim()}` : false)
            .filter(e => e)
            .join('&');
        console.log(this.query);
        this.toPage(1);
    }
    private toPage(number) {
        return this._productService.searchProduct(
            `select=brief/details/status/updatedAt/is_active&limit=10&page=${number}&${this.query}`
        ).subscribe(
            (data) => {
                console.log(data);
                this.totalPages = Math.ceil(parseInt(data['total'], 10) / parseInt(data['limit'], 10));
                this.source.load(data['docs'].map(product => {
                    this.origin = data['docs'];
                    return flatten()(product);
                }));
            },
            (err) => errorHandler(err)
        );
    }
    private async multiSelect(data) {
        this.productIdPendingApproval = data.selected.map(e => e._id);
    }
    private approveAll() {
        this.changeProductStatus('Approved');
    }
    private rejectAll() {
        this.changeProductStatus('Rejected');
    }
    private statusChange(status) {
        this.searchForm.value['status'] = status;
        this.searchProduct();
    }
}
