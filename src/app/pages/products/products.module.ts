import { PaginationComponent } from '../../theme/util-components/pagination/pagination.component';
import { CategoryService } from './cate/cate.service';
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';
import { DragulaModule } from 'ng2-dragula';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { DirectivesModule } from '../../theme/directives/directives.module';
import { CateComponent } from './cate/cate.component';
import { ButtonViewComponent } from './product-list/button-view.component';
import { ButtonApproveComponent } from './product-list/button-approve.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductNewComponent } from './product-detail/product-detail.component';
import { ProductService } from './products.service';
import { ImageModalComponent } from './product-detail/image-modal/image-modal.component';
import { UtilComponentsModule } from 'app/theme/util-components/util.module';
import { VariantOptionsComponent } from 'app/pages/products/variant-options/variant-options.component';
import { VariantOptionsService } from 'app/pages/products/variant-options/variant-options.service';
import { ButtonDetailComponent } from 'app/pages/products/product-list/button-detail.component';



export const routes = [
  { path: '', redirectTo: 'product-list', pathMatch: 'full' },
  { path: 'product-list', component: ProductListComponent, data: { breadcrumb: 'Products List' } },
  { path: 'product-detail/:id', component: ProductNewComponent, data: { breadcrumb: 'Products Detail' } },
  { path: 'cate', component: CateComponent, data: { breadcrumb: 'Products Categories' } },
  { path: 'variant-options', component: VariantOptionsComponent, data: { breadcrumb: 'Variants Options' } }

];

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    RouterModule.forChild(routes),
    DragulaModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
  ],
  declarations: [
    ProductListComponent,
    CateComponent,
    ProductNewComponent,
    ButtonViewComponent,
    ButtonApproveComponent,
    ButtonDetailComponent,
    ImageModalComponent,
    PaginationComponent,
    VariantOptionsComponent
  ],
  providers: [ProductService, CategoryService, VariantOptionsService]
})

export class ProductsModule { }
