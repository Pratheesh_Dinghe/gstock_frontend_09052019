import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { AuthService } from '../../shared/auth.service';


@Injectable()
export class ProductService {
  private url = environment.apiUrl;
  private headers;

  constructor(
    private http: HttpClient,
    private _authService: AuthService
  ) {
    this.headers = _authService.getHeaders();
  }
  public searchUsersByUserId(body) {
    return this.http.post(this.url + '/admin/searchusersbyuserid', body, { headers: this.headers });
  }

  public getCategory(cate, level) {
    return this.http.get(this.url + '/product_category/cat/' + cate + '/level/' + level, { headers: this.headers });
  }
  public postMaitnCate(body) {
    return this.http.post(this.url + '/product_category/' + body, { headers: this.headers });
  }
  public getProduct(id) {
    return this.http.get(this.url + '/admin/product/search/?select=brief/detail/variants&product_id=' + id);
  }
  public getSingleProduct(id) {
    return this.http.get(this.url + '/admin/product/detail?product_id=' + id);
  }
  public getProductVariant(productId) {
    return this.http.get(this.url + '/admin/product/variant/?product_id=' + productId);
  }
  public searchProduct(query) {
    return this.http.get(`${this.url}/admin/product/search?${query}`);
  }
  public changeProductStatus(status, id) {
    return this.http.put(`${this.url}/admin/product/status`, {
      product_id: id,
      status: status
    });
  }
  public updateProduct(productId, body) {
    return this.http.put(`${this.url}/admin/product?product_id=${productId}`, body);
  }
  public deleteProduct(productId) {
    return this.http.delete(`${this.url}/admin/product?product_id=${productId}`, { headers: this.headers, responseType: 'text' });
  }
}
