import { errorHandler } from '../../../_helper/errorHandler';
import { Component, ViewEncapsulation } from '@angular/core';

import { VariantOptionsService } from './variant-options.service';

@Component({
  selector: 'az-cate',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './variant-options.component.html',
  styleUrls: ['./variant-options.component.scss'],
})
export class VariantOptionsComponent {
  private settings = {
    add: {
      confirmCreate: true,
    },
    delete: {
      confirmDelete: true,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      name: {
        title: 'Option name'
      },
      value: {
        title: 'Option value'
      }
    },
  };
  private data = [];
  constructor(
    public _variantOptionsService: VariantOptionsService
  ) {
    this.loadTable();
  }
  private loadTable() {
    /* Load source and perform a callback once loaded */
    return this._variantOptionsService.getAllVariantOptions().subscribe(
      (data: any) => {
        console.log(data);
        const holder = [];
        data.forEach(e => {
          e['value'].forEach(d => {
            holder.push({
              'name': e['name'],
              'value': d
            });
          });
        });
        this.data = holder;
        console.log(this.data);
      },
      err => errorHandler(err)
    );
  }

  private onCreateConfirm(event) {
    this._variantOptionsService.createNewVariantOptions({ 'title': 'variant_options', ...event['newData'] })
      .subscribe(
      onSuccess => {
        console.log(onSuccess);
        event.confirm.resolve();
      },
      err => errorHandler(err)
      );
  }
  private onEditConfirm(event) {
    const oldData = event.data;
    const newData = event.newData;
    this._variantOptionsService.updateVariantOptions(
      '?title=variant_options&name=' + oldData['name'] + '&value=' + oldData['value'],
      { 'title': 'variant_options', ...event['newData'] })
      .subscribe(
      onSuccess => {
        console.log(onSuccess);
        event.confirm.resolve();
      },
      err => errorHandler(err)
      );
  }
  private onDeleteConfirm(event) {
    const data = event.data;
    this._variantOptionsService.deleteVariantOptions(
      '?title=variant_options&name=' + data['name'],
      { 'value': data['value'] })
      .subscribe(
      onSuccess => {
        console.log(onSuccess);
        event.confirm.resolve();
      },
      err => errorHandler(err)
      );
  }


}
