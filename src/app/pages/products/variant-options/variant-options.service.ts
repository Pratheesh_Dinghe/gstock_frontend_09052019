import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { AuthService } from '../../../shared/auth.service';




@Injectable()
export class VariantOptionsService {

    private url = environment.apiUrl;
    private headers;

    constructor(
        private http: HttpClient,
        private _authService: AuthService
    ) {
        this.headers = _authService.getHeaders();
    }

    getAllVariantOptions() {
        return this.http.get(environment.apiUrl + '/admin/variants');
    }
    createNewVariantOptions(body) {
        return this.http.post(environment.apiUrl + '/admin/variants', body);
    }
    updateVariantOptions(query, body) {
        return this.http.put(environment.apiUrl + '/admin/variants/' + query, body);
    }
    deleteVariantOptions(query, body) {
        return this.http.post(environment.apiUrl + '/admin/variants/delete' + query, body);
    }
}
