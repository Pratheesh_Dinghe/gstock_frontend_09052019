import { errorHandler } from '../../../_helper/errorHandler';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { PromotionService } from '../promotion.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatepickerOptions } from 'ng2-datepicker';
import { ChangeDetectorRef } from '@angular/core';
import { CategoryService } from './../../products/cate/cate.service';
import { UserService } from './../../user/user/user.service';

interface Promotion {
    name: string;
    detail: string;
    promo_code: string;
    start: Date;
    end: Date;
    status: string;
    target: [
        {
            _id: string,
            store:
            {
                name: string
            }
        }
    ];
    kind: string;
    /**
     * ["baby product","book"] / [merchantID]
     */
    promo_type: string;
    value: string;
    min_order: boolean;
    min_value: number;
    limit: number;
}

@Component({
    selector: 'az-promotion-detail',
    // encapsulation: ViewEncapsulation.None,
    templateUrl: './promotion-detail.component.html',
    styleUrls: ['./promotion-detail.scss'],
    providers: [CategoryService, UserService]
})

export class PromotionDetailComponent implements OnInit {
    private discountForm: FormGroup;
    private options: DatepickerOptions;
    private promotionID: string;
    private newPromotion = false;
    private status: string;
    private promoType = [
        {
            name: 'Percentage discount',
            value: 'pd'
        },
        {
            name: 'Fixed Amount',
            value: 'fa'
        },
        {
            name: 'Free Shipment',
            value: 'fs'
        }
    ];
    private target = [];
    private result: Array<string>;
    constructor(
        router: Router,
        public _promotionService: PromotionService,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _ref: ChangeDetectorRef,
        private _categoryService: CategoryService,
        private _userService: UserService
    ) {

        this.discountForm = formBuilder.group({
            name: [, Validators.required],
            detail: [, Validators.required],
            promo_code: [, Validators.required],
            kind: ['storewide', Validators.required],
            value: [0, Validators.required],
            promo_type: [, Validators.required],
            first_order: [false],
            min_order: [false],
            min_value: [0],
            start: [new Date(), Validators.required],
            status: [],
            end: [new Date(Date.now() + 10000000000), Validators.required]
        });

        this.options = {
            minYear: 1970,
            maxYear: 2030,
            displayFormat: 'MMM D[,] YYYY HH[:]MM[:]ss',
            barTitleFormat: 'MMMM YYYY',
            firstCalendarDay: 0 // 0 - Sunday, 1 - Monday
        };
    }

    submit() {
        this._promotionService.createPromotion(
            {
                target: this.target.map(t => t.merchant_id),
                ...this.discountForm.value
            }).subscribe(
                (onSuccess: any) => alert(onSuccess.message),
                onErr => errorHandler(onErr)
            );
    }
    update() {
        if (!this.discountForm.valid) { return alert('Incomplete form'); }
        this._promotionService.updatePromotion(this.promotionID, {
            ...this.discountForm.value,
            target: this.target.map(t => t.merchant_id)
        }).subscribe(
            onSuccess => alert(onSuccess),
            onErr => errorHandler(onErr)
        );
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            if (!params['id']) { return this.newPromotion = true; }
            this.promotionID = params['id'];
            this._promotionService.getAllPromotion('?promotion_id=' + this.promotionID)
                .subscribe(
                    (data: Promotion) => {
                        console.log(data);
                        this.status = data.status;
                        this.target = data.target.map(t => ({
                            merchant_id: t._id,
                            store: t.store.name,
                        }));
                        this.discountForm.patchValue(data);
                    },
                    err => errorHandler(err)
                );
        });
    }
    private switchState() {
        this._promotionService.updatePromotionStatus(this.promotionID).subscribe(
            (s: any) => this.status = s.status,
            error => errorHandler(error)
        );
    }
    private search(keyword) {
        this._userService.getMerchantByStoreName(keyword).subscribe(
            (s: any) => {
                this.result = s.docs.map(n => ({
                    merchant_id: n._id,
                    store: n.store.name
                }));
                console.log(this.result);
            },
            err => errorHandler(err)
        );
        // this._categoryService.getAllCategory(keyword).subscribe(
        //     (s: any) => this.result = s,
        //     err => errorHandler(err)
        // );
    }
    private selectResult(result) {
        const store = this.target.map(t => t.store);
        const index = store.indexOf(result.store);
        if (index < 0) {
            this.target.push(result);
        } else {
            this.target.splice(index, 1);
        }
    }
}
