import { errorHandler } from '../../../_helper/errorHandler';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalDataSource, Ng2SmartTableModule } from 'ng2-smart-table';
import { PromotionService } from '../promotion.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common'
@Component({
    selector: 'az-promotion-list',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './promotion-list.component.html',
    styleUrls: ['./promotion-list.component.scss'],
    providers: [DatePipe]
})
export class PromotionListComponent implements OnInit {
    private source = new LocalDataSource();
    private settings = {
        actions: {
            add: false,
            edit: false,
            delete: false

        },


        columns: {
            promo_code: {
                title: 'Promo Code',

            },
            status: {
                title: 'Status'
            },
            used: {
                title: 'Used',

            },
            start: {
                title: 'start',
                valuePrepareFunction: (start) => {
                    var raw = new Date(start);
                    var formatted = this.datePipe.transform(raw, 'yyyy-MM-dd');
                    return formatted;
                }

            },
            end: {
                title: 'end',
                valuePrepareFunction: (date) => {
                    var raw = new Date(date);
                    var formatted = this.datePipe.transform(raw, 'yyyy-MM-dd');
                    return formatted;
                }

            }

        }

    };
    private promoData = [];

    constructor(private router: Router, private _promotionService: PromotionService, private datePipe: DatePipe) {
    }

    ngOnInit() {
        this._promotionService.getAllPromotion('?status=All').subscribe(
            (data: any) => {
                this.source.load(data);
            },
            err => errorHandler(err)
        );
    }

    private statusChange(status?) {
        this._promotionService.getAllPromotion('?status=' + status).subscribe(
            (data: any) => this.source.load(data),
            err => errorHandler(err)
        );

    }



    private selectPromo(event): void {
        this.router.navigate(['pages/promotion/promotion-detail/' + event.data._id]);
    }


}
