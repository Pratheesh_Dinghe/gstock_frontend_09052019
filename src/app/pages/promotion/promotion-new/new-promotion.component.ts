import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PromotionService } from '../promotion.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'az-new-promotion',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './new-promotion.component.html',
    styleUrls: ['./new-promotion.component.scss']
})

export class NewPromotionComponent implements OnInit {
   private discountForm1: FormGroup;
   private discountForm2: FormGroup;
    private discountForm3: FormGroup;

    private orderID: string;
    public router: Router;
    private disOption=[
        {name:"Percentage discount",
        value:"pd"},
        {name:"Fixed Amount",
        value:"fa"},
        {name:"Free Shipment",
        value:"fs"}
    ];
    private settings = {
        columns: {
            itemId: {
                title: 'Item ID'
            },
            itemName: {
                title: 'Item Name'
            },
            price: {
                title: 'Price'
            },
            orderQty: {
                title: 'Qty'
            },
            totalAmount: {
                title: 'Total'
            }
        }
    };
    constructor(
        router: Router,
        public _promotionService: PromotionService,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder
    ) {

        this.router = router;
        this.discountForm1=formBuilder.group({
            discountCode:[''],
            discountOption:[''],
            discountValue:[0],
            miniOrder:[false],
            miniValue:[0]
        });
        this.discountForm2=formBuilder.group({
           disApplies:[''],
            disSearch:['']
        });

        this.discountForm3=formBuilder.group({
            startDate:[''],
            startTime:['']
        });


    }

    ngOnInit() {

        this.activatedRoute.params.subscribe((params: Params) => {
            if (!!!params) return
            this.orderID = params['id'];
            console.log('OrderID ', this.orderID);
        })
        this.getOrderDetail();
    }

    private getOrderDetail(): void {
        // this.data = this._promotionService.getOrderDetail();
    }

}
