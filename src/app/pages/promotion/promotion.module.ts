import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PromotionService } from './promotion.service';
import { PromotionListComponent } from './promotion-list/promotion-list.component';
import { NewPromotionComponent } from './promotion-new/new-promotion.component';
import { PromotionDetailComponent } from './promotion-detail/promotion-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { NgDatepickerModule } from 'ng2-datepicker';

export const routes = [
  { path: '', redirectTo: 'promotion-list', pathMatch: 'full' },
  { path: 'promotion-list', component: PromotionListComponent, data: { breadcrumb: 'Promotion List' } },
  { path: 'promotion-new', component: PromotionDetailComponent, data: { breadcrumb: 'New Promotion' } },
  { path: 'promotion-detail/:id', component: PromotionDetailComponent, data: { breadcrumb: 'Promotion Detail' } },

  // { path: 'promotion-detail/:id', component: PromotionDetailComponent, data: { breadcrumb: 'Promotion Detail' } },

];

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    AgmCoreModule,
    RouterModule.forChild(routes),
    Ng2SmartTableModule,
    NgDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [PromotionListComponent, NewPromotionComponent, PromotionDetailComponent],
  providers: [PromotionService]
})

export class PromotionModule { }
