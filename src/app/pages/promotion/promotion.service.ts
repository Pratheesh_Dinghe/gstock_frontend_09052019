import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { AuthService } from 'app/shared/auth.service';


@Injectable()
export class PromotionService {
  private url = environment.apiUrl;
  private headers;

  constructor(
    private http: HttpClient,
    private _auth: AuthService
  ) {
    this.headers = _auth.getHeaders();
  }

  public getAllPromotion(body) {
    return this.http.get(`${this.url}/admin/promotion${body}`);
  }

  public createPromotion(body) {
    return this.http.post(this.url + '/admin/promotion', body);
  }
  public updatePromotion(promotionID, body) {
    return this.http.put(`${this.url}/admin/promotion?promotion_id=${promotionID}`, body);
  }
  public updatePromotionStatus(promotionID) {
    return this.http.put(`${this.url}/admin/promotion/status?promotion_id=${promotionID}`, {});
  }
}
