import { NgModule } from '@angular/core';
import { RewardPointMallComponent } from './reward-point-mall.component';
import { RouterModule } from '@angular/router';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
export const routes = [
    { path: '', component: RewardPointMallComponent, pathMatch: 'full' }
];
@NgModule({
    imports: [
        RouterModule.forChild(routes),
        // Ng2SmartTableModule,
        // ReactiveFormsModule,
        // FormsModule
    ],
    exports: [],
    declarations: [RewardPointMallComponent],
})

export class RewardPointMallModule { }
