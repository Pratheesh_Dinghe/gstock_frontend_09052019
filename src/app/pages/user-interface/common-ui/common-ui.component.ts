import { errorHandler } from '../../../_helper/errorHandler';
import { Component, ChangeDetectorRef, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DragulaService } from 'ng2-dragula/components/dragula.provider';
import { CommonUiService } from 'app/pages/user-interface/common-ui/common-ui.service';
import { environment } from 'environments/environment';
import { addTimestamp } from '../../../_helper/addTimestamp';

const IMAGE_SPECIFICATION = {
  banner: {
    width: 1000,
    height: 500,
  },
  brand: {
    width: 175,
    height: 50,
  },
  icon: {
    width: 500,
    height: 5000,
  }
};
const IMAGE_FORM_KEY_TABLE = {
  banner: {
    fieldName: 'adsImg',
    url: 'ads-image'
  },
  brand: {
    fieldName: 'brandImg',
    url: 'brand-image'
  },
  icon: {
    fieldName: 'iconImg',
    url: 'icon-image'
  }
};
@Component({
  selector: 'az-common-ui',
  templateUrl: './common-ui.component.html',
  styleUrls: ['./common-ui.component.scss'],
  providers: [CommonUiService]
})
export class CommonUIComponent implements OnInit {
  // title:common-ui/floor_one name:setting value:[]
  // title:common-ui/floor_two name:image value:[]
  // title:common-ui/floor-order name:order value:[floor_one,.......]
  @ViewChild('bannerUpload') bannerUpload: ElementRef;
  @ViewChild('brandUpload') brandUpload: ElementRef;
  @ViewChild('iconUpload') iconUpload: ElementRef;

  private apiUrl = environment.apiUrl;
  private imageChosen;
  private order = [];
  private copyOrder = [];
  private floors;
  private floorSelected;

  private banners = [];
  private bannerRoutes = ['', '', ''];

  private brands = [];
  private brandNames = ['', '', '', '', ''];
  constructor(
    private _dragulaService: DragulaService,
    private _commonUiService: CommonUiService,
    private _changeDetectorRef: ChangeDetectorRef
  ) {

    for (let i = 0; i <= 2; i++) { this.banners.push(addTimestamp(`${this.apiUrl}/uploads/uploads/home/ads/${i + 1}.png`), ); }
    for (let i = 0; i <= 4; i++) { this.brands.push(addTimestamp(`${this.apiUrl}/uploads/uploads/home/brand/${i + 1}.png`), ); }
    _commonUiService.getFloors().subscribe(
      (floors: Array<any>) => {
        this.order = floors.find(e => 'floor_order' === e.name).value;
        this.copyOrder = this.order.map((o, i) => addTimestamp(`${this.apiUrl}/uploads/uploads/home/icon/${o}.png`));
        this.floors = this.order.map(o => floors.find(x => x.name === 'floor_' + o));
      }
    );
    _commonUiService.getBrands().subscribe((brands: any) => this.brandNames = brands.value);
    _commonUiService.getAdsLink().subscribe((link: any) => this.bannerRoutes = link.value);
    _dragulaService.over.subscribe(
      e => {
        console.log(this.copyOrder);
        // _changeDetectorRef.detectChanges();
      }
    );

  }

  ngOnInit() {

  }
  private upload(e, type) {
    e.preventDefault();
    const { width: rWidth, height: rHeight } = IMAGE_SPECIFICATION[type];
    const { fieldName, url } = IMAGE_FORM_KEY_TABLE[type];
    const file = e.target.files[0];
    if (!file) { return; }
    const img = new Image();
    img.src = window.URL.createObjectURL(file);
    img.onload = () => {
      const detectedWidth = img.naturalWidth,
        detectedHeight = img.naturalHeight;
      if (
        detectedWidth < rWidth &&
        detectedHeight < rHeight) { return alert(`Image must be at least ${rWidth}X${rHeight}. Detected ${detectedWidth}X${detectedHeight}`); }
      window.URL.revokeObjectURL(img.src);
      const formData = new FormData();
      formData.append('whichImage', this.imageChosen);
      formData.append(fieldName, e.target.files[0]);
      console.log(`Uploading ${type}`, e);
      this._commonUiService.postImage(url, formData).subscribe(
        (s: any) => {
          switch (type) {
            case 'banner':
              this.updateBanner();
              break;
            case 'brand':
              this.updateBrand();
              break;
            case 'icon':
              this.updateIcon();
              break;
            default: break;
          }
          alert(s.message);
        },
        err => errorHandler(err));
    };
  }

  updateBanner() {
    this.banners[this.imageChosen - 1] = addTimestamp(this.banners[this.imageChosen - 1]);
    console.log(this.banners);
  }
  updateBrand() {
    this.brands[this.imageChosen - 1] = addTimestamp(this.brands[this.imageChosen - 1]);
  }
  updateIcon() {
    console.log(this.copyOrder[this.floorSelected], this.imageChosen);
    this.copyOrder[this.floorSelected] = addTimestamp(this.copyOrder[this.floorSelected]);
  }
  triggerImageUpload(e, i, type) {
    console.log('this.imageChosen', this.imageChosen);
    e.preventDefault();
    let inputElement: HTMLInputElement;
    switch (type) {
      case 'banner':
        this.imageChosen = i;
        inputElement = this.bannerUpload.nativeElement;
        break;
      case 'brand':
        this.imageChosen = i + 1;
        inputElement = this.brandUpload.nativeElement;
        break;
      case 'icon':
        this.imageChosen = this.copyOrder[i].split('.png')[0].slice(-1) / 1;
        this.floorSelected = i;
        console.log('this.copyOrder', this.copyOrder);
        console.log('this.imageChosen', this.imageChosen);
        inputElement = this.iconUpload.nativeElement;
        break;
      default: break;
    }
    inputElement.click();
  }


  private prepareModal(floor) {
    if (6 === floor.value.length - 1) { return; }
    this.floorSelected = floor;
    (<any>$('#sub')).modal('show');
  }
  // private add(newSub) {
  //   this._commonUiService.addFloor({
  //     name: this.floorSelected.name,
  //     value: newSub.value
  //   }).subscribe(
  //     onSuccess => {
  //       alert(onSuccess);
  //       this.floorSelected.value.push(newSub.value);
  //     },
  //     onErr => errorHandler(onErr)
  //   );
  // }
  private updateFloor(floor, i, input) {
    const name = floor.name;
    const oldValue = floor.value[i];
    const newValue = input.value;
    if (oldValue === newValue) { return; }
    this._commonUiService.updateFloor(name, oldValue, { name: name, value: newValue }).subscribe(
      onSuccess => {
        alert(onSuccess);
        floor.value[i] = newValue;
      },
      onErr => errorHandler(onErr));
  }

  private updateOrder() {
    this._commonUiService.updateFloorOrder({ order: this.order }).subscribe(
    (s: any) => alert(s.message),
      err => errorHandler(err)
    );
  }

  private updateBannerRoutes() {
    this._commonUiService.updateAdsLink({ ads_link: this.bannerRoutes }).subscribe(
      (s: any) => alert(s.message),
      err => errorHandler(err)
    );
  }
  private updateBrands() {
    this._commonUiService.updateBrands({ brands: this.brandNames }).subscribe(
      (s: any) => alert(s.message),
      err => errorHandler(err)
    );
  }

}
