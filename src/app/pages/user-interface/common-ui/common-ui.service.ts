import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
const api = environment;
@Injectable()
export class CommonUiService {
  constructor(
    private _httpClient: HttpClient
  ) {

  }
  public getBrands() {
    return this._httpClient.get(`${api.homeUrl}/brands`);
  }
  public updateBrands(body) {
    return this._httpClient.put(`${api.homeUrl}/brands`, body);
  }

  public addFloor(body) {
    return this._httpClient.post(`${api.floorUrl}`, body, { responseType: 'text' });
  }
  public getFloors() {
    return this._httpClient.get(api.floorUrl);
  }
  public updateFloor(name, value, body) {
    return this._httpClient.put(`${api.floorUrl}?name=${encodeURIComponent(name)}&value=${encodeURIComponent(value)}`, body, { responseType: 'text' });
  }
  public updateFloorOrder(body) {
    return this._httpClient.put(`${api.floorUrl}/order`, body);
  }

  public postImage(url, body) {
    return this._httpClient.post(`${api.homeUrl}/${url}`, body);
  }
  public updateAdsLink(body) {
    return this._httpClient.put(`${api.homeUrl}/ads-link`, body);
  }
  public getAdsLink() {
    return this._httpClient.get(`${api.homeUrl}/ads-link`);
  }
}
