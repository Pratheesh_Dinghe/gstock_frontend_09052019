import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { CommonUIComponent } from './common-ui/common-ui.component';
import { RouterModule } from '@angular/router';
import { DragulaModule } from 'ng2-dragula';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
export const routes = [
  { path: '', component: CommonUIComponent },

  { path: 'home', component: CommonUIComponent, data: { breadcrumb: 'Home Page UI' }, },

];
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    DragulaModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CommonUIComponent]
})
export class UserInterfaceModule { }
