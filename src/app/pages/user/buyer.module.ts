import { UserModule } from './user/user.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from './../../theme/directives/directives.module';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
export const routes = [
    // { path: '', component: UserComponent },
    { path: 'newbuyer', component: UserDetailComponent, data: { breadcrumb: 'Create New Buyer' }, },

    { path: 'detail/:id', component: UserDetailComponent, data: { breadcrumb: 'Buyer Detail' } },


];

@NgModule({
    imports: [
        UserModule,
        CommonModule,
        DirectivesModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        Ng2SmartTableModule,
    ],
})

export class BuyerModule { }
