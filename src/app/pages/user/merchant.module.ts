import { UserModule } from './user/user.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from './../../theme/directives/directives.module';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserComponent } from './user/user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TreeModule } from "angular-tree-component"
export const routes = [
    // { path: '', component: UserComponent },

    { path: 'newmerchant', component: UserDetailComponent, data: { breadcrumb: 'Create New Merchant' }, },

    { path: 'detail/:id', component: UserDetailComponent, data: { breadcrumb: 'Merchant Detail' } },

];

@NgModule({
    imports: [
        CommonModule,
        DirectivesModule,
        UserModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        Ng2SmartTableModule,
        TreeModule

    ],
})

export class MerchantModule { }
