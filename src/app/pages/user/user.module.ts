import { UserModule } from './user/user.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserService } from './user/user.service';

export const routes = [
    { path: '', redirectTo: 'buyer', pathMatch: 'full' },
    {
        path: 'buyer', loadChildren: 'app/pages/user/buyer.module#BuyerModule', data: { breadcrumb: 'Buyer' },
    },
    {
        path: 'merchant', loadChildren: 'app/pages/user/merchant.module#MerchantModule', data: { breadcrumb: 'Merchant' }
    },
    // { path: 'merchant/detail/:id', component: DetailComponent, data: { breadcrumb: 'Seller Detail' } },


];

@NgModule({
    imports: [
        UserModule,
        RouterModule.forChild(routes),
    ],
    providers: [UserService]
})

export class UserManagementModule { }
