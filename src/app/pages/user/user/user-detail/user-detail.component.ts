import { errorHandler } from '../../../../_helper/errorHandler';
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { SlicePipe, Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { PagesService } from '../../../pages.service';
import { CategoryService } from 'app/pages/products/cate/cate.service';
import { environment } from 'environments/environment';


@Component({
    selector: 'az-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss'],
})
export class UserDetailComponent implements OnInit {
    public myForm: FormGroup;
    private userID = '';
    private user_group = '';
    private baseUrl;
    public isActive;
    public options = {
        minYear: 1970,
        maxYear: 2001,
        displayFormat: 'MMM D[,] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 0
    };
    private settings = {
        actions: {
            add: false,
            edit: false,
            delete: false

        },
        columns: {
            number: {
                title: 'No.',
            },
            cate: {
                title: 'Main Cate'
            },
            value: {
                title: '% Commission',
            }

        }
    };
    private exisitingCommission = [];
    private commissionPendingDeletion = [];
    private searchForm: FormGroup;
    private infoCollection = {
        product_count: 0
    };
    private formArray;
    private creditWalletHistory = [];
    constructor(
        private _formBuilder: FormBuilder,
        private _activatedRoute: ActivatedRoute,
        private _fb: FormBuilder,
        private _location: Location,
        private _userService: UserService,
        private _categoryService: CategoryService
    ) {
    }

    ngOnInit() {
        this._activatedRoute.data.subscribe(data => {
            this.user_group = data['breadcrumb'] === 'Buyer Detail' ? 'buyer' : 'merchant';
            this.formInit(this.user_group);
            this.getNavParams()
                .subscribe((id) => {
                    if (!id) { return; }
                    this.userID = id;
                    this.baseUrl = `${environment.apiUrl}/uploads/user/${id}/`;
                    this.getUserDetail(id).subscribe(() => { },
                        (err) => errorHandler(err));
                    if ('buyer' === this.user_group) {
                        this.getCreditWallet(id).subscribe(() => { },
                            (err) => errorHandler(err));
                    }
                });
        });

    }
    private category;
    private formInit(userGroup) {
        const form = {
            'credential': this._fb.group({
                email: [, [Validators.required, Validators.minLength(5)]],
                password: [, [Validators.required, Validators.minLength(5)]],
                confirmPassword: [, [Validators.required, Validators.minLength(5)]],
                user_group: [this.user_group, [Validators.required]]
            }),
            'profile': this._fb.group({
                gender: [],
                DOB: [new Date()],
                prefix: [],
                suffix: [],
                first_name: [],
                middle_name: [],
                last_name: [],
                profile_pic: [],
            }),
            'bank': this._fb.group({
                name: [],
                account_no: []
            }),
        };
        const formArray = [{
            formGroupDisplayName: 'Credential',
            formGroupName: 'credential',
            formControls: [
                {
                    displayName: 'Email',
                    formControlName: 'email',
                    required: true,
                },
                {
                    displayName: 'Password',
                    formControlName: 'password',
                    required: true,
                    type: 'password'
                },
                {
                    displayName: 'Confirm password',
                    formControlName: 'confirmPassword',
                    required: true,
                    type: 'password'
                },
            ]
        }, {
            formGroupDisplayName: 'Profile',
            formGroupName: 'profile',
            formControls: [
                {
                    displayName: 'Gender',
                    formControlName: 'gender',
                    type: 'select',
                    value: ['Male', 'Female'],
                    required: false,
                },
                {
                    displayName: 'DOB',
                    formControlName: 'DOB',
                    type: 'date',
                    required: false,
                },
                {
                    displayName: 'Prefix',
                    formControlName: 'prefix',
                    type: 'select',
                    value: ['Mr.', 'Mrs.', 'Ms.', 'Dr.'],
                    required: false,
                },
                {
                    displayName: 'Suffix',
                    formControlName: 'suffix',
                    required: false,
                },
                {
                    displayName: 'First name',
                    formControlName: 'first_name',
                    required: false,
                },
                {
                    displayName: 'Middle name',
                    formControlName: 'middle_name',
                    required: false,
                },
                {
                    displayName: 'Last name',
                    formControlName: 'last_name',
                    required: false,
                },
                {
                    displayName: 'Profile picture',
                    formControlName: 'profile_pic',
                    required: false,
                },
            ]
        },
        {
            formGroupDisplayName: 'Bank Related',
            formGroupName: 'bank',
            formControls: [
                {
                    displayName: 'Bank Name',
                    formControlName: 'name'
                },
                {
                    displayName: 'Account Number',
                    formControlName: 'account_no'
                },
            ]
        }];
        console.log(userGroup);
        if ('merchant' === userGroup) {
            form['store'] = this._fb.group({
                name: [],
                description: []
            });
            formArray.push({
                formGroupDisplayName: 'Store Related',
                formGroupName: 'store',
                formControls: [
                    {
                        displayName: 'Store Name',
                        formControlName: 'name'
                    },
                ]
            });
        }
        this.formArray = formArray;
        this.myForm = this._fb.group(form);
        this.searchForm = this._fb.group({
            'category': [],
            'rate': [],
            'default': [],
        });
        this.searchForm.controls['category'].valueChanges.debounceTime(600).subscribe(value => {
            if ('' === value.trim()) {
                this.category = [];
                return;
            }
            this._categoryService.getAllCategory(value).subscribe(
                (d: any) => this.category = d
            );
        });
    }
    private select(a, defaultV) {
        this.searchForm.controls['category'].setValue(a, { emitEvent: false });
        this.searchForm.controls['default'].setValue(defaultV, { emitEvent: false });
        this.category = [];
    }
    private selectForDelete(c, i) {
        this.commissionPendingDeletion[i] = this.commissionPendingDeletion[i] ? undefined : c._id;
    }
    private confirmDelete() {
        this._userService.deleteMerchantComm({
            category: this.commissionPendingDeletion.filter(c => c),
            merchant_id: this.userID
        }).subscribe(
            (s: any) => {
                this.commissionPendingDeletion = [];
                this.exisitingCommission = this.formatCommission(s.commission);
            },
            e => errorHandler(e)
        );
    }
    private setCommission() {
        this._userService.updateMerchantComm({ ...this.searchForm.value, merchant_id: this.userID }).subscribe(
            (s: any) => this.exisitingCommission = this.formatCommission(s.commission),
            err => errorHandler(err)
        );
    }
    private getNavParams() {
        return this._activatedRoute.params.map((params: Params) => params.id || null);
    }
    private getUserDetail(id) {
        return this._userService.getUserDetail(id)
            .map(
                (userDetails: any) => {
                    const docs = userDetails.docs;
                    if (!docs.length) { return; }
                    const { product_count, is_active, commission, credential } = docs[0];
                    this.isActive = is_active;
                    if (commission) { this.exisitingCommission = this.formatCommission(commission); }
                    this.infoCollection.product_count = product_count;
                    this.user_group = credential.user_group;
                    Object.keys(docs[0]).forEach((field) => this.patchForm(field, docs[0][field]));
                });
    }
    private getCreditWallet(id) {
        return this._userService.getCreditWalletHistory(id).map(
            (res: any) => {
                this.creditWalletHistory = res.data.history;
            }
        );
    }
    private formatCommission(commission) {
        return commission.map(
            (c) => ({
                path: c.category_id.path,
                _id: c.category_id._id,
                default: c.category_id.commission,
                rate: c.rate
            })
        );
    }
    private dateConversion(dateInString) {
        if (dateInString !== undefined) { return new Date(Date.parse(dateInString)).toISOString().substr(0, 10); }
    }
    private patchForm(field, value) {
        const control = this.myForm.get(field);
        if (control) { return control.patchValue(value); }
    }
    private getAllErr(form) {
        return Object.keys(form.controls).map(field => {
            if (typeof (form.controls[field].value) === 'object') {
                return this.getAllErr(form.get(field));
            } else {
                return form.get(field).errors;
            }
        });
    }
    private onSubmit() {
        this.getNavParams().subscribe((id) => {
            if (id) { return this.updateUser(id); }
            return this.createUser();
        });

    }
    private createUser() {
        return this._userService.createUser(this.myForm.value)
            .subscribe(
                (onSuccess) => alert(onSuccess),
                (onErr) => errorHandler(onErr)
            );
    }
    private updateUser(id) {
        return this._userService.updateUserDetail(id, this.myForm.value)
            .subscribe(
                (onSuccess: any) => alert(onSuccess.message),
                (onErr) => errorHandler(onErr)
            );
    }
    private checkError(form, subform) {
        if (this.myForm.get(form).get(subform).dirty) {
            return this.myForm.get(form).get(subform).hasError('required');
        }
    }
    private changeStatus() {
        const action = this.isActive ? 'suspend' : 'activate';
        const warning = `Are you sure you want to procceed to ${action} ${this.user_group}?`;
        if (!confirm(this.user_group === 'merchant' ? `This action will also ${action} merchant\'s products. ` + warning : warning)) { return; }
        this._userService.changeUserStatus(!this.isActive, this.user_group, this.userID).subscribe(
            (onSuccess) => this.isActive = !this.isActive,
            (onErr) => errorHandler(onErr)
        );
    }
    private deleteAccount() {
        if (!confirm('**Warning** You are performing an irreversible action. Are you sure to proceed?')) { return; }
        this._userService.deleteAccount(this.userID).subscribe(
            (s) => {
                alert('User deleted');
                this._location.back();
            },
            (e) => errorHandler(e)
        );
    }
    private updateCreditWallet(sign, amount, memo) {
        if (Number.isNaN(amount.value * 100 / 100)) { return; }
        this._userService.updateCreditWallet(this.userID, {
            amount: '+' === sign.value ? amount.value : -amount.value,
            memo: memo.value
        }).subscribe(
            (res: any) => this.getCreditWallet(this.userID).subscribe(),
            (error) => errorHandler(error)
        );
    }
}
