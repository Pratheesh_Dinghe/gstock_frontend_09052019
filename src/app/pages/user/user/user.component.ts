import { LocalDataSource } from 'ng2-smart-table';
import { errorHandler } from '../../../_helper/errorHandler';
import { UserService } from './user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PagesService } from '../../pages.service';
import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, Renderer2, ChangeDetectorRef } from '@angular/core';
import flatten from 'flatten-obj';
import { DatepickerOptions } from 'ng2-datepicker';
@Component({
    selector: 'az-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    providers: [UserService]
})
export class UserComponent implements AfterViewInit {
    private user_group = '';
    @ViewChild('smartTable', { read: ElementRef }) smartTable: ElementRef;
    private settings: any = {
        pager: {
            display: true,
        },
        actions: false,
        class: 'table-bordered',
    };
    private data = new LocalDataSource();
    constructor(
        private _userService: UserService,
        private _rd2: Renderer2,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
    ) {
    }
    ngAfterViewInit() {
        this._activatedRoute.data.subscribe(
            data => {
                if (data['breadcrumb'] === 'Buyer') {
                    this.user_group = 'buyer';
                    this.settings = {
                        ...this.settings,
                        columns: {
                            'credential.email': {
                                title: 'Email'
                            },
                            'profile.last_name': {
                                title: `${data['breadcrumb']} Name`
                            },
                            'createdAt': {
                                title: 'Created on'
                            },
                            'is_active': {
                                title: 'Active'
                            },
                        }
                    };
                } else {
                    this.user_group = 'merchant';
                    this.settings = {
                        ...this.settings,
                        columns: {
                            'credential.email': {
                                title: 'Email'
                            },
                            'profile.last_name': {
                                title: `${data['breadcrumb']} Name`
                            },
                            'store.name': {
                                title: 'Store name'
                            },
                            'createdAt': {
                                title: 'Created on'
                            },
                            'is_active': {
                                title: 'Active'
                            },
                        }
                    };
                }
            }
        );
        this._userService.getUsers(this.user_group)
            .subscribe(
                (onSuccess: any) => {
                    this.data.setPaging(1, 40, true);
                    // console.log(onSuccess.docs.length);
                    if (onSuccess.docs.length) { return this.data.load(onSuccess.docs.map(doc => flatten()(doc))); }
                    alert('No user found');
                },
                (err) => errorHandler(err),
        );
        return this.addBorder();
    }
    private addBorder() {
        const table: HTMLElement = this.smartTable.nativeElement.getElementsByTagName('table')[0];
        const tableHeads: Array<HTMLElement> = this.smartTable.nativeElement.getElementsByTagName('th');
        this._rd2.addClass(table, 'table-bordered');
        for (let i = 0; i < tableHeads.length; i++) {
            this._rd2.addClass(tableHeads[i], 'text-center');
        }
    }
    private toCreate() {
        switch (this.user_group) {
            case 'buyer':
                return this._router.navigate(['pages/usermanagement/buyer/newbuyer']);
            case 'merchant':
                return this._router.navigate(['pages/usermanagement/merchant/newmerchant']);
            default: return;
        }
    }
    private toUserDetail(event) {
        if (event.isSelected) {
            switch (this.user_group) {
                case 'buyer':
                    return this._router.navigate(['pages/usermanagement/buyer/detail/' + event.data.id]);
                case 'merchant':
                    return this._router.navigate(['pages/usermanagement/merchant/detail/' + event.data.id]);
                default: return;
            }
        }
    }
}
