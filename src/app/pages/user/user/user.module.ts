import { UserDetailComponent } from './user-detail/user-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from '../../../theme/directives/directives.module';

import { UserComponent } from '../user/user.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgDatepickerModule } from 'ng2-datepicker';
export const routes = [
    { path: '', component: UserComponent },


];

@NgModule({
    imports: [
        CommonModule,
        DirectivesModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        Ng2SmartTableModule,
        NgDatepickerModule,
    ],
    declarations: [
        UserComponent,
        UserDetailComponent
    ]
})

export class UserModule { }
