import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'app/shared/auth.service';

@Injectable()
export class UserService {
    private url = environment.apiUrl;
    private header;
    constructor(
        private httpClient: HttpClient,
        private _authService: AuthService
    ) {
        this.header = _authService.getHeaders();
    }

    public getUsers(querystring) {
        return this.httpClient.get(`${this.url}/admin/user/detail?limit=1000&offset=0&user_group=${querystring}`, this.header);
    }
    public getUserDetail(id) {
        return this.httpClient.get(this.url + '/admin/user/detail?_id=' + id, this.header);
    }
    public getMerchantByStoreName(storeName) {
        return this.httpClient.get(`${this.url}/admin/user/detail?store_name=${storeName}`, this.header);
    }
    public updateUserDetail(id, body) {
        return this.httpClient.put(this.url + '/admin/user/detail?_id=' + id, body, this.header);
    }
    public createUser(body) {
        return this.httpClient.post(this.url + '/admin/user/create', body, this.header);
    }

    public updateMerchantComm(body) {
        return this.httpClient.put(`${this.url}/admin/user/commission`, body, this.header);
    }
    public deleteMerchantComm(body) {
        return this.httpClient.post(`${this.url}/admin/user/commission/delete`, body, this.header);
    }
    public changeUserStatus(activate, user_group, userId) {
        const url = activate ? `${this.url}/admin/user/activate/${user_group}?_id=${userId}` : `${this.url}/admin/user/suspend/${user_group}?_id=${userId}`;
        return this.httpClient.put(url, {}, this.header);
    }
    public getCreditWalletHistory(buyer_id) {
        return this.httpClient.get(`${this.url}/admin/user/user/credit/${buyer_id}`, this.header);
    }

    public getCreditWalletList() {
        return this.httpClient.get(`${this.url}/admin/user/user/credit_list`, this.header);
    }

    public updateCreditWallet(buyer_id, body) {
        return this.httpClient.post(`${this.url}/admin/user/user/credit/${buyer_id}`, body, this.header);
    }
    public deleteAccount(user_id) {
        return this.httpClient.delete(`${this.url}/admin/user?_id=${user_id}`, this.header);
    }
}
