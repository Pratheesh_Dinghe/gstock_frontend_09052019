import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppState } from 'app/app.state';
@Injectable()
export class AuthService {
    private userId = localStorage.getItem('userId');
    private url = environment.apiUrl;
    // store the URL so we can redirect after logging in
    public redirectUrl: string;
    constructor(
        public http: HttpClient,
        public _appState: AppState
    ) {
    }

    public setHeaders(token) {
        localStorage.setItem('__Gstock_token__', token);
        return new HttpHeaders({ 'Content-type': 'json', 'authorization': 'bearer ' + token });
    }
    public getHeaders() {
        const token = localStorage.getItem('__Gstock_token__');
        return new HttpHeaders({ 'Content-type': 'json', 'authorization': 'bearer ' + token });
    }

    public setUser(userId) {
        localStorage.setItem('userId', userId);
    }

    public getUser() {
        const userId = localStorage.getItem('userId');
        if (!userId) {
            return alert('Please login again');
        }
        return userId;
    }


    public login(body) {
        return this.http.post(this.url + '/admin/login', body);
    }

    public logout() {
        this._appState.notifyDataChanged('login', false);
        localStorage.clear();
        return;
    }

    public signup(body) {
        return this.http.post(this.url + '/admin/register', body);
    }

    public authState() {
        if (!this.getHeaders()) {
            return Observable.throw('Please Login');
        }
        return this.http.get(this.url + '/admin/auth', { headers: this.getHeaders() });
    }
}




