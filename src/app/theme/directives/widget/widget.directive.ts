import { Directive, ElementRef, OnInit } from '@angular/core';
import 'widgster';

@Directive({
  selector: '[widget]'
})

export class Widget implements OnInit {
  $el: any;

  constructor(el: ElementRef) {
    this.$el = $(el.nativeElement);
    (<any>$.fn).widgster.Constructor.DEFAULTS.bodySelector = '.widget-body';

    $(document).on('close.widgster', (e) => {
      const $colWrap = $(e.target).closest(' [class*="col-"]:not(.widget-container)');
      if (!$colWrap.find('.widget').not(e.target).length) {
        $colWrap.remove();
      }
    });

    $(document).on('fullscreened.widgster', (e) => {
      $(e.target).find('div.widget-body').addClass('scrolling');
    }).on('restored.widgster', (e) => {
      $(e.target).find('div.widget-body').removeClass('scrolling');
    });
  }

  ngOnInit(): void {
    this.$el.widgster();
  }
}

