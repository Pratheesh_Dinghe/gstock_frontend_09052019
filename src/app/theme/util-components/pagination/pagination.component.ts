import { EventEmitter, Input, Component, ViewEncapsulation, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'az-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
  @Output('number') public numberEmitter: EventEmitter<number> = new EventEmitter();
  @Input('jump') public jump: number;
  @Input('totalPages') set newTotalPages(totalPages: number) {
    this.head = 0;
    this.pages = totalPages;
    this.display = this.pages > this.jump ?
      this.shiftDigit(0, this.jump) :
      this.shiftDigit(0, this.pages - this.head);

  }
  private pages = 0;
  private head = 0;
  private display: Array<number> = [];
  constructor() {
  }
  private transitTo(direction) {
    switch (direction) {
      case 'previous': {
        if (this.head !== 0) {
          this.head -= this.jump;
          this.display = this.shiftDigit(this.head, this.jump);
        }
        break;
      }
      case 'next': {
        if (this.pages - this.head > this.jump * 2) {
          this.head += this.jump;
          this.display = this.shiftDigit(this.head, this.jump);
        } else {
          this.head = this.pages - this.pages % this.jump;
          this.display = this.shiftDigit(this.head, this.pages % this.jump);
        }
        //  remainingPages > this.jump * 2 ?
        break;
      }
      case 'first': {
        this.head = 0;
        this.display = this.shiftDigit(this.head, this.jump);
        break;
      }
      case 'last': {
        this.head = this.pages - this.pages % this.jump;
        this.display = this.shiftDigit(this.head, this.pages % this.jump);
        break;
      }
      default: break;
    }
  }
  private navigateToPage(number) {
    this.numberEmitter.emit(number);
  }
  private shiftDigit(head: number, box: number) {
    const array = [];
    for (let i = 1; i <= box; i++) {
      array.push(head + i);
    }
    return array;
  }
}
