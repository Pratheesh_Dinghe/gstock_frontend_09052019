// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// const apiUrl = 'http://13.251.24.195/admin.gstock';
// const apiUrl = 'http://localhost/admin.gstock';
// const apiUrl = "https://gstockadminservices.glux.sg";
const apiUrl = "http://localhost:3000";

// const apiUrl = 'https://gstock.sg:7200/admin.gstock';
const uiOptionsUrl = `${apiUrl}/admin/attributes/ui-options`;
const floorUrl = `${uiOptionsUrl}/floor`;
const homeUrl = `${uiOptionsUrl}/home`;
const imageBaseUrl = `${apiUrl}/uploads`;
export const environment = {
  production: false,
  apiUrl,
  uiOptionsUrl,
  floorUrl,
  homeUrl
  // imageBaseUrl
  // apiUrl: 'https://gstock.sg/admin.gstock',
  // apiUrl: 'https://api.admin.gstock.com',
};
